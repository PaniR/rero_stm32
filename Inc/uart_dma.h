/** @addtogroup  group_uart_dma UART DMA
 *  @{
 */

/**
 * \brief 			UART DMA ring buffer definitions
 * \details			Efficient ring buffer implementation for UART interface utilizing DMA support.
 * \author 			Reinhard Panhuber
 * \version 		1.0.0
 * \date 			04.10.2019
 * \file 			uart_dma.h
 * \copyright  		MIT
 */

#ifndef UART_DMA_H_
#define UART_DMA_H_

// Buffer length definition
#define UART_DMA_RX_BUFFER_LEN 32 	//!< RX buffer length - Maximum is 2^32 / 2 = 2^31, however since the MCU does not has so much memory it does not really matter
#define UART_DMA_TX_BUFFER_LEN 32 	//!< TX buffer length - Maximum is 2^32 / 2 = 2^31, however since the MCU does not has so much memory it does not really matter

// Sanity checks
#if UART_DMA_RX_BUFFER_LEN > 0x80000000
#error UART_DMA: RX buffer size too big!
#endif

#if UART_DMA_TX_BUFFER_LEN > 0x80000000
#error UART_DMA: TX buffer size too big!
#endif

// Setup of definitions for efficient power of two index handling
// Be aware: if buffer length is not a power of two we limit the index space to be at most 2*buffer_length - thus we can avoid expansive modulo operations, however, overflows are recognized only for one overrun i.e. you must update write pointer at least once every 2*depth writes.
// If UART_DMA_RX_BUFFER_LEN is a power of two
///\cond

#if ((UART_DMA_RX_BUFFER_LEN & (UART_DMA_RX_BUFFER_LEN - 1)) == 0)

#define UART_DMA_RX_BUFFER_LEN_POW_2 					//!< Flag indicating that RX buffer length is a power of two
#define UART_DMA_RX_BUFFER_LEN_MASK  (UART_DMA_RX_BUFFER_LEN - 1) 		//!< Length mask for quick modulo calculation of RX pointers

#else

#define UART_DMA_RX_BUFFER_MAX_PT_IDX                       2*UART_DMA_RX_BUFFER_LEN - 1                      //!< Maximum absolute RX pointer index - limited to be at most two times buffer size to allow for a faster modulo substitute
#define UART_DMA_RX_BUFFER_NON_USED_IDX_SPACE 	          0xFFFFFFFF - UART_DMA_RX_BUFFER_MAX_PT_IDX 	//!< Remaining index space not usable for absolute RX index

#endif

// If UART_DMA_TX_BUFFER_LEN is a power of two
#if ((UART_DMA_TX_BUFFER_LEN & (UART_DMA_TX_BUFFER_LEN - 1)) == 0)

#define UART_DMA_TX_BUFFER_LEN_POW_2 					//!< Flag indicating that TX buffer length is a power of two
#define UART_DMA_TX_BUFFER_LEN_MASK  (UART_DMA_TX_BUFFER_LEN - 1) 		//!< Length mask for quick modulo calculation of TX pointers

#else

#define UART_DMA_TX_BUFFER_MAX_PT_IDX                       2*UART_DMA_TX_BUFFER_LEN - 1                      //!< Maximum absolute TX pointer index - limited to be at most two times buffer size to allow for a faster modulo substitute
#define UART_DMA_TX_BUFFER_NON_USED_IDX_SPACE 		0xFFFFFFFF - UART_DMA_TX_BUFFER_MAX_PT_IDX 	//!< Remaining index space not usable for absolute TX index


#endif

///\endcond

/* Includes ------------------------------------------------------------------*/
#include "stm32f3xx_hal.h" 					// Only needed for assert_param function
#include "string.h"
#include "stdint.h"

/// UART DMA state definitions
typedef enum
{
	UART_DMA_OK, 				          //!< Return value for normal operation
	UART_DMA_ERROR_INIT, 				//!< Non successful init
	UART_DMA_RX_ERROR_OVERFLOW,				//!< RX buffer overflow - take care this may no indicate all overflows since the DMA might write so many bytes into the buffer such that the write pointer index gets an overflow - which for 32 bit of size_t takes a lot of bytes to copy and thus is extremely unlikely
	UART_DMA_TX_ERROR_OVERFLOW 				//!< TX buffer overflow - no space left to copy further bytes into it
} uart_dma_status_t;

/// UART DMA control struct
typedef struct {
	volatile size_t rAbsRX;                                     //!< Absolute read pointer of RX buffer
	volatile size_t wAbsRX; 				//!< Absolute write pointer of RX buffer
	volatile size_t wRelOldRX; 				//!< Relative old write pointer of RX buffer needed to determine the amount of data got by DMA
	volatile size_t rAbsTX;                                     //!< Absolute read pointer of TX buffer - This variable gets updated outside of ISR so has not to be volatile
	volatile size_t wAbsTX; 				//!< Absolute write pointer of TX buffer
	uint8_t * restrict pBuffTX; 			          //!< Pointer to TX buffer - Necessary for HAL function
	uint16_t nNewBytes; 				//!< Number of new inserted bytes, is set to zero once bytes in TX buffer are sent by uart_dma_send() - uint16 is sufficient since the maximum allowable size of the buffer is 2^16-1
	volatile uint16_t nBytesSending; 		          //!< Number of bytes currently sending - uint16_t is enough since the maximum allowable size of the buffer is 2^16-1
	volatile uint16_t nBytesToSend; 		          //!< Number of bytes to be sent by DMA - uint16_t is enough since the maximum allowable size of the buffer is 2^16-1
	volatile uint16_t nBytesSent; 			//!< Number of bytes to sent by DMA - uint16_t is enough since the maximum allowable size of the buffer is 2^16-1
	UART_HandleTypeDef *huart; 				//!< Pointer to UART handle
} uart_dma_t;

// Init function
uart_dma_status_t uart_dma_init(uart_dma_t *hudx, UART_HandleTypeDef *huart, uint8_t * pBuffRX, uint8_t * pBuffTX);

/** @name ISR Callback Functions
 *  Callback functions required for RX and TX operations.
 *  @{
 */

// Function to be called in case of UART RxIdle ISRs
// TAKE CARE: For STM32F0 and STM32F3 for sure the idle ISR fires immediately after startup
// which can not be prevented. Take special care uart_dma_rx_check_idle() is not called for
// this first case. This bug is not present on STM32F4 devices, for the rest it is unknown to me.
uart_dma_status_t uart_dma_update_RxIdle(uart_dma_t *hudx);

// Function to be called in case of UART RxCplt ISR
uart_dma_status_t uart_dma_update_RxCplt(uart_dma_t *hudx);

// Function to be called in case of UART TxHalfCplt ISR
void uart_dma_update_TxHalfCplt(uart_dma_t *hudx);

// Function to be called in case of UART TxCplt ISR
void uart_dma_update_TxCplt(uart_dma_t *hudx);

///@}

// Functions regarding RX

/** @name RX Functions
 *  Functions regarding RX buffer.
 *  @{
 */

size_t uart_dma_cnt_rx(uart_dma_t *hudx);
size_t uart_dma_free_rx(uart_dma_t *hudx);
size_t uart_dma_cnt_rel_to_ref_rx(uart_dma_t *hudx, size_t offset, size_t ref);
size_t uart_dma_read_rx(uart_dma_t *hudx, size_t offset, void* data, size_t len);
size_t uart_dma_peek_rx(uart_dma_t *hudx, size_t offset, void* data, size_t len);
size_t uart_dma_peek_by_ref_rx(uart_dma_t *hudx, size_t offset, size_t ref, void* data, size_t n);
size_t uart_dma_peek_byte_rx(uart_dma_t *hudx, size_t offset, uint8_t *data);
size_t uart_dma_get_linear_block_length_rx(uart_dma_t *hudx, size_t offset);
size_t uart_dma_get_linear_block_length_by_ref_rx(uart_dma_t *hudx, size_t offset, size_t ref);
void * uart_dma_get_linear_block_address_rx(uart_dma_t *hudx, size_t offset);
void * uart_dma_get_linear_block_address_by_ref_rx(uart_dma_t *hudx, size_t offset, size_t ref);
size_t uart_dma_modify_rx(uart_dma_t *hudx, size_t offset, void* data, size_t len);
size_t uart_dma_write_1_rx(uart_dma_t *hudx, void* data, size_t len);
uart_dma_status_t uart_dma_write_2_rx(uart_dma_t *hudx, void* data, size_t len);
uart_dma_status_t uart_dma_put_rx(uart_dma_t *hudx, uint8_t data);
void uart_dma_modify_byte_rx(uart_dma_t *hudx, size_t offset, uint8_t data);
uart_dma_status_t uart_dma_reserve_space_rx(uart_dma_t *hudx, size_t len, size_t * pAddr);
uart_dma_status_t uart_dma_write_2_by_ref_rx(uart_dma_t *hudx, size_t offset, void* data, size_t len, size_t addr);
void uart_dma_put_by_ref_rx(uart_dma_t *hudx, size_t offset, uint8_t data, size_t ref);
uint8_t uart_dma_peek_byte_by_ref_rx(uart_dma_t *hudx, size_t offset, size_t ref);
size_t uart_dma_flush_rx(uart_dma_t *hudx, size_t len);
size_t uart_dma_delete_rx(uart_dma_t *hudx, size_t len);
void uart_dma_get_linear_read_info_rx(uart_dma_t *hudx, size_t offset, size_t *len1, size_t * len2, void ** p1, void ** p2);

///@}

/** @name TX Functions
 *  Functions regarding TX buffer and sending.
 *  @{
 */

size_t uart_dma_cnt_tx(uart_dma_t *hudx);
size_t uart_dma_free_tx(uart_dma_t *hudx);
size_t uart_dma_cnt_rel_to_ref_tx(uart_dma_t *hudx, size_t offset, size_t ref);
size_t uart_dma_read_tx(uart_dma_t *hudx, size_t offset, void* data, size_t len);
size_t uart_dma_peek_tx(uart_dma_t *hudx, size_t offset, void* data, size_t len);
size_t uart_dma_peek_by_ref_tx(uart_dma_t *hudx, size_t offset, size_t ref, void* data, size_t n);
size_t uart_dma_peek_byte_tx(uart_dma_t *hudx, size_t offset, uint8_t *data);
size_t uart_dma_get_linear_block_length_tx(uart_dma_t *hudx, size_t offset);
size_t uart_dma_get_linear_block_length_by_ref_tx(uart_dma_t *hudx, size_t offset, size_t ref);
void * uart_dma_get_linear_block_address_tx(uart_dma_t *hudx, size_t offset);
void * uart_dma_get_linear_block_address_by_ref_tx(uart_dma_t *hudx, size_t offset, size_t ref);
size_t uart_dma_modify_tx(uart_dma_t *hudx, size_t offset, void* data, size_t len);
size_t uart_dma_write_1_tx(uart_dma_t *hudx, void* data, size_t len);
uart_dma_status_t uart_dma_write_2_tx(uart_dma_t *hudx, void* data, size_t len);
uart_dma_status_t uart_dma_put_tx(uart_dma_t *hudx, uint8_t data);
void uart_dma_modify_byte_tx(uart_dma_t *hudx, size_t offset, uint8_t data);
uart_dma_status_t uart_dma_reserve_space_tx(uart_dma_t *hudx, size_t len, size_t * pAddr);
uart_dma_status_t uart_dma_write_2_by_ref_tx(uart_dma_t *hudx, size_t offset, void* data, size_t len, size_t addr);
void uart_dma_put_by_ref_tx(uart_dma_t *hudx, size_t offset, uint8_t data, size_t ref);
uint8_t uart_dma_peek_byte_by_ref_tx(uart_dma_t *hudx, size_t offset, size_t ref);
size_t uart_dma_flush_tx(uart_dma_t *hudx, size_t len);
size_t uart_dma_delete_tx(uart_dma_t *hudx, size_t len);
void uart_dma_get_linear_read_info_tx(uart_dma_t *hudx, size_t offset, size_t *len1, size_t * len2, void ** p1, void ** p2);
size_t uart_dma_send(uart_dma_t *hudx);

///@}

#endif /* UART_DMA_H_ */

/** @} */
