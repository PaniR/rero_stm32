/** @addtogroup  group_rero RERO
 *  @{
 */

/**
 * \brief 			Rero transmission protocol definitions
 * \details			Definitions of the rero transmission protocol. A Python implementation is also [available](https://pypi.org/project/rero/).
 * \author 			Reinhard Panhuber
 * \version 		1.0.0
 * \date 			04.10.2019
 * \file 			rero.h
 * \copyright  		MIT
 */

#ifndef RERO_H_
#define RERO_H_

#include <uart_dma.h>
#include "string.h"
#include "stdint.h"
#include "stm32f3xx_hal.h"

#define RERO_AddFrameFieldLen 	1										//!< Length of additional frame length field in bytes - 		range: 0 <= RERO_AddFrameFieldLen <= 3
#define RERO_AddrFieldLen 		2										//!< Length of address field in bytes - 						range: 0 <= RERO_AddrFieldLen <= 4
#define RERO_CRCLen 			4 										//!< CRC length in bytes - 									range: 0 <= RERO_CRCLen <= 8
#define RERO_FrameLenCheck 		1 										//!< Enable checksum for frame length field 					range: 0 <= RERO_FrameLenCheck <= 1

///\cond
#define RERO_EOF 0x00 													// End of file delimiter - DO NOT CHANGE!

// This macros help to determine the number of bits needed to represent a number. Works for number of bits <= 32. Source: https://stackoverflow.com/questions/27581671/how-to-compute-log-with-the-preprocessor

#define IS_REPRESENTIBLE_IN_D_BITS(D, N)                \
		(((unsigned long) N >= (1UL << (D - 1)) && (unsigned long) N < (1UL << D)) ? D : -1)

#define BITS_TO_REPRESENT(N)                            \
		(N == 0 ? 1 : (31                                     \
				+ IS_REPRESENTIBLE_IN_D_BITS( 1, N)    \
				+ IS_REPRESENTIBLE_IN_D_BITS( 2, N)    \
				+ IS_REPRESENTIBLE_IN_D_BITS( 3, N)    \
				+ IS_REPRESENTIBLE_IN_D_BITS( 4, N)    \
				+ IS_REPRESENTIBLE_IN_D_BITS( 5, N)    \
				+ IS_REPRESENTIBLE_IN_D_BITS( 6, N)    \
				+ IS_REPRESENTIBLE_IN_D_BITS( 7, N)    \
				+ IS_REPRESENTIBLE_IN_D_BITS( 8, N)    \
				+ IS_REPRESENTIBLE_IN_D_BITS( 9, N)    \
				+ IS_REPRESENTIBLE_IN_D_BITS(10, N)    \
				+ IS_REPRESENTIBLE_IN_D_BITS(11, N)    \
				+ IS_REPRESENTIBLE_IN_D_BITS(12, N)    \
				+ IS_REPRESENTIBLE_IN_D_BITS(13, N)    \
				+ IS_REPRESENTIBLE_IN_D_BITS(14, N)    \
				+ IS_REPRESENTIBLE_IN_D_BITS(15, N)    \
				+ IS_REPRESENTIBLE_IN_D_BITS(16, N)    \
				+ IS_REPRESENTIBLE_IN_D_BITS(17, N)    \
				+ IS_REPRESENTIBLE_IN_D_BITS(18, N)    \
				+ IS_REPRESENTIBLE_IN_D_BITS(19, N)    \
				+ IS_REPRESENTIBLE_IN_D_BITS(20, N)    \
				+ IS_REPRESENTIBLE_IN_D_BITS(21, N)    \
				+ IS_REPRESENTIBLE_IN_D_BITS(22, N)    \
				+ IS_REPRESENTIBLE_IN_D_BITS(23, N)    \
				+ IS_REPRESENTIBLE_IN_D_BITS(24, N)    \
				+ IS_REPRESENTIBLE_IN_D_BITS(25, N)    \
				+ IS_REPRESENTIBLE_IN_D_BITS(26, N)    \
				+ IS_REPRESENTIBLE_IN_D_BITS(27, N)    \
				+ IS_REPRESENTIBLE_IN_D_BITS(28, N)    \
				+ IS_REPRESENTIBLE_IN_D_BITS(29, N)    \
				+ IS_REPRESENTIBLE_IN_D_BITS(30, N)    \
				+ IS_REPRESENTIBLE_IN_D_BITS(31, N)    \
				+ IS_REPRESENTIBLE_IN_D_BITS(32, N)    \
		)                                      \
		)

#define RERO_FirstCobsCodeNBits BITS_TO_REPRESENT(RERO_AddFrameFieldLen+RERO_AddrFieldLen+RERO_CRCLen+2)		// Number of bits needed for the COBS length code of CRC
#define RERO_FirstCobsCodeMask 	(0xff << (8-RERO_FirstCobsCodeNBits))	// Mask for frame length
#define RERO_FirstManByteMask 	(0xff >> RERO_FirstCobsCodeNBits)		// Mask for frame first management byte

///\endcond

// Conduct some checks

#if RERO_AddFrameFieldLen > 3
#error RERO: Number of additional frame length bytes must be <= 3
#endif

#if RERO_FrameLenCheck > 1
#error RERO: FrameLenCheck must be 0 or 1
#endif

#if RERO_AddrFieldLen > 4
#error RERO: Number of address bytes must be <= 4
#endif

#if RERO_CRCLen > 8
#error RERO: Number of CRC bytes must be <= 8
#endif

#if RERO_AddFrameFieldLen + RERO_AddrFieldLen + RERO_CRCLen > 13
#error RERO: Number of management bytes must be <= 13
#endif

/// Rero state definitions
typedef enum
{
	RERO_OK, 						//!< Indicates normal operation i.e. valid state with no errors
	RERO_OTHER_REC, 				//!< Received message intended for a different node
	RERO_ERROR_INIT, 				//!< Non successful init
	RERO_ERROR_CRC, 				//!< CRC error
	RERO_ERROR_TIMEOUT,				//!< Timeout
	RERO_ERROR_SYNC,				//!< EOF delimiter bytes did not occur on given index - resynchronizations required
	RERO_ERROR_TX_NO_SPACE 			//!< TX buffer has no sufficient space to hold encoded RERO frame
} rero_status_t;

/// States for receive state machine
typedef enum {
	RERO_Rec_Header, 				//!< Receive header of rero frame state
	RERO_Rec_Rest, 					//!< Receive rest of rero frame (after header) state
	RERO_Check_Frame, 				//!< Check received frame state
	RERO_Frame_Available, 			//!< Frame available state - frame was received and checks were successful
	RERO_Out_Of_Sync 				//!< Out of synchronization state - resynchronization is conducted automatically
} rero_state_t;

/// Rero control struct
typedef struct
{
	rero_state_t sms; 				//!< State of Receive State Machine
	CRC_HandleTypeDef *hcrc; 		//!< Handle of CRC unit
	uart_dma_t *hudx; 				//!< Handle of Buffer
	TIM_HandleTypeDef *htim; 		//!< Handle of timer
	size_t address; 				//!< Address of node
	size_t nBytesRcvd; 				//!< Number of bytes received
	size_t frameLen;				//!< Length of received encoded payload
	size_t frameLenDeco;			//!< Length of received decoded payload
	uint8_t frameDecodedFlag;		//!< Frame decoded flag
	size_t iterIdx;					//!< Index for iteration
	size_t iterLim;					//!< Limit for iteration index
	size_t headerBuffRef;			//!< Frame header buffer reference
	size_t cobsCodeRef;				//!< COBS code buffer reference
	uint8_t iterCobsCode;			//!< Limit for iteration index
	uint8_t iterFlag;				//!< COBSR flag for iteration
	uint8_t * iterPtr;				//!< Pointer for iteration
	uint8_t searchLen;				//!< Encoding search length
	volatile uint8_t timeoutFlag;	//!< Timeout happened flag
#if RERO_CRCLen > 0
	size_t decoCobsCode; 			//!< Cobs code needed to decode CRC
#endif
} rero_t;

/** @name ISR Callback Functions
 *  Callback functions required for rero receive state machine.
 *  @{
 */

void rero_timeout_notification(rero_t *hrero);

///@}

rero_status_t rero_init(rero_t *hrero, CRC_HandleTypeDef *hcrc, uart_dma_t *hudr, TIM_HandleTypeDef *htim6, size_t address);
rero_status_t rero_check_rx(rero_t *hrero);
size_t rero_is_frame_available(rero_t *hrero);
size_t rero_decode_frame_in_buffer(rero_t *hrero);
void rero_notify_frame_processed(rero_t *hrero);
size_t rero_copy_frame(rero_t *hrero, void* data);
void rero_get_linear_read_info(rero_t *hrero, size_t *len1, size_t * len2, void ** p1, void ** p2);
size_t rero_get_decoded_frame_length(rero_t *hrero);
uint8_t rero_iterate(rero_t *hrero, uint8_t * byteOut);
rero_status_t rero_encode(rero_t * hrero, uint8_t data);
rero_status_t rero_send(rero_t * hrero, uint32_t addr);

#endif /* RERO_H_ */

/** @} */
