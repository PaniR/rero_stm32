/** @addtogroup  group_uart_dma UART DMA
 *  @{
 */

/**
 * \brief 			UART DMA ring buffer implementation
 * \details			Efficient ring buffer implementation for UART interface utilizing DMA support.
 * \author 			Reinhard Panhuber
 * \version 		1.0.0
 * \date 			04.10.2019
 * \file 			uart_dma.c
 * \copyright  		MIT
 */

#include <uart_dma.h>
#include "main.h"

static size_t advance_pointer_rx(size_t p, size_t offset);
static size_t backward_pointer_rx(size_t p, size_t offset);
static size_t get_relative_pointer_rx(size_t p, size_t offset);

static size_t advance_pointer_tx(size_t p, size_t offset);
static size_t backward_pointer_tx(size_t p, size_t offset);
static size_t get_relative_pointer_tx(size_t p, size_t offset);

static size_t uart_dma_peek(size_t offset, void* data, size_t n, size_t rRel, uint8_t * pBuff, size_t buffLen, size_t cnt);
static size_t uart_dma_peek_byte(size_t offset, uint8_t *data, size_t rRel, uint8_t * pBuff, size_t cnt);
static size_t uart_dma_modify(size_t offset, void* data, size_t len, size_t wRelTmp, uint8_t * pBuff, size_t buffLen);
static void uart_dma_write(void * data, size_t len, size_t wRelTmp, uint8_t * pBuff, size_t buffLen);
//static size_t uart_dma_write(void * restrict data, size_t len, size_t wRelTmp, uint8_t * restrict pBuff, size_t buffLen, size_t cnt);

static void uart_dma_get_linear_read_info(size_t *len1, size_t * len2, void ** p1, void ** p2, size_t wRel, size_t rRel, uint8_t * pBuff, size_t buffLen, size_t cnt);

/**
 * \brief           Initialize UART DMA construct
 * \param[in]       hudx: Pointer to UART DMA structure
 * \param[in]       huart: Pointer to UART handle
 * \param[in]       pBuffRX: Pointer to RX buffer array
 * \param[in]       pBuffTX: Pointer to TX buffer array
 * \return          UART_DMA_OK on success, UART_DMA_ERROR_INIT otherwise
 */
uart_dma_status_t uart_dma_init(uart_dma_t *hudx, UART_HandleTypeDef *huart, uint8_t * pBuffRX, uint8_t * pBuffTX)
{
	assert_param(hudx != NULL);

	if (hudx == NULL) {
		return UART_DMA_ERROR_INIT;
	}

	memset(hudx, 0, sizeof(*hudx));
	hudx->huart = huart;

	// Setup RX
	// Establish DMA to collect data from UART
	if (HAL_UART_Receive_DMA(huart, pBuffRX, UART_DMA_RX_BUFFER_LEN) != HAL_OK)
	{
		assert_param(0);
		return UART_DMA_ERROR_INIT;
	}

	// Clear idle flag
	// __HAL_UART_CLEAR_FLAG(huart, UART_FLAG_IDLE);

	// Enable idle interrupt
	__HAL_UART_ENABLE_IT(huart,UART_IT_IDLE);

	// Setup TX
	// We let the HAL do the job - this may be inefficient but for the sake of generic interfaces we do that
	hudx->pBuffTX = pBuffTX;

	return UART_DMA_OK;
}

/**
 * \brief 			Update RX buffer in case of RxIdle interrupt
 *
 * 					Check for new data received with DMA in case of RX idle interrupt and save status
 * 					TAKE CARE: For STM32F0 and STM32F3 for sure the idle ISR fires immediately after startup
 * 					which can not be prevented. Take special care uart_dma_rx_check_idle() is not called for
 * 					this first case. This bug is not present on STM32F4 devices, for the rest it is unknown to me.
 * \param[in]       hudx: Pointer to UART DMA structure
 *
 * \return          UART_DMA_OK on success, UART_DMA_RX_ERROR_OVERFLOW in case of buffer overflow
 */

uart_dma_status_t uart_dma_update_RxIdle(uart_dma_t *hudx)
{
	// Calculate current position in buffer - CNDTR will never be zero here since it is reseted to UART_DMA_RX_BUFFER_LEN immediately

#if defined(STM32F303x8)
	size_t pos = UART_DMA_RX_BUFFER_LEN - hudx->huart->hdmarx->Instance->CNDTR;
#endif

#if defined(STM32F411xE)
	size_t pos = UART_DMA_RX_BUFFER_LEN - hudx->huart->hdmarx->Instance->NDTR;
#endif

	size_t offset = pos - hudx->wRelOldRX;

	hudx->wAbsRX = advance_pointer_rx(hudx->wAbsRX, offset);
	hudx->wRelOldRX = pos;

	// Check for overflow - this is not completely sure to work in every case, however it is very likely to detect an overflow
	if (uart_dma_cnt_rx(hudx) > UART_DMA_RX_BUFFER_LEN)
	{
		return UART_DMA_RX_ERROR_OVERFLOW;
	}

	return UART_DMA_OK;
}

/**
 * \brief           Check for new data received with DMA in case of RX complete interrupt and save status
 * \param[in]       hudx: Pointer to UART DMA structure
 * \return          UART_DMA_OK on success, UART_DMA_RX_ERROR_OVERFLOW in case of buffer overflow
 */
uart_dma_status_t uart_dma_update_RxCplt(uart_dma_t *hudx)
{
	size_t offset = UART_DMA_RX_BUFFER_LEN - hudx->wRelOldRX;
	hudx->wAbsRX = advance_pointer_rx(hudx->wAbsRX, offset);
	hudx->wRelOldRX = 0;

	// Check for overflow - this is not completely sure to work in every case, however it is very likely to detect an overflow
	if (uart_dma_cnt_rx(hudx) > UART_DMA_RX_BUFFER_LEN)
	{
		return UART_DMA_RX_ERROR_OVERFLOW;
	}

	return UART_DMA_OK;
}

/**
 * \brief           Update TX read pointer in case of TxHalfCplt interrupt
 * \param[in]       hudx: Pointer to UART DMA structure
 */
void uart_dma_update_TxHalfCplt(uart_dma_t *hudx)
{
	// Determine offset
	size_t offset = hudx->nBytesSending / 2;

	// Advance pointer
	hudx->rAbsTX = advance_pointer_tx(hudx->rAbsTX, offset);
}

/**
 * \brief           Update TX read pointer for TxCplt interrupt
 * \param[in]       hudx: Pointer to UART DMA structure
 */
void uart_dma_update_TxCplt(uart_dma_t *hudx)
{
	// Work on local variables
	uint16_t nBytesToSend = hudx->nBytesToSend;
	uint16_t nBytesSent = hudx->nBytesSent;
	uint16_t nBytesSending = hudx->nBytesSending;

	// Determine offset
	size_t offset = nBytesSending - nBytesSending / 2;

	// Advance pointer
	hudx->rAbsTX = advance_pointer_tx(hudx->rAbsTX, offset);

	// Check if we have to send something else
	if ((nBytesToSend - nBytesSent) > 0) 		// THIS IS IMPORTANT TO WRITE IT THIS WAY!!	hudx->nBytesToSend > hudx->nBytesSent WOULD BE WRONG DUE TO OVERFLOW ERROR - THIS WAY IT WORKS ALSO IN CASE OF OVERFLOW!
	{
		// Determine linear length part
		uint16_t linLen = (uint16_t) uart_dma_get_linear_block_length_tx(hudx, 0);

		// Clip if necessary - this occurs if in the meanwhile new bytes were put into TX buffer
		if ((nBytesSent + linLen - nBytesToSend) > 0)
		{
			linLen = nBytesToSend - nBytesSent;
		}

		// Update
		hudx->nBytesSent += linLen;
		hudx->nBytesSending = linLen;

		// Get current address
		uint8_t * pBuff = uart_dma_get_linear_block_address_tx(hudx, 0);

		// Start transmission
		HAL_UART_Transmit_DMA(hudx->huart, pBuff, linLen);

		// Alternative: For sending only wrapped part
		//		HAL_UART_Transmit_DMA(hudx->huart, hudx->pBuffTX, hudx->nBytesToSend);
	}
}

/**
 * \brief           Get number of bytes in RX buffer
 * \param[in]       hudx: Pointer to UART DMA structure
 * \return          Number of bytes in RX buffer
 */
size_t uart_dma_cnt_rx(uart_dma_t *hudx)
{
	size_t w, r, cnt;

	// Operate on temporary values in case they change in between
	w = hudx->wAbsRX;
	r = hudx->rAbsRX;

	cnt = w-r;

#ifndef UART_DMA_RX_BUFFER_LEN_POW_2
	if (r > w) cnt -= UART_DMA_RX_BUFFER_NON_USED_IDX_SPACE;
#endif

	return cnt;
}

/**
 * \brief           Get number of bytes in TX buffer
 * \param[in]       hudx: Pointer to UART DMA structure
 * \return          Number of bytes in TX buffer
 */
size_t uart_dma_cnt_tx(uart_dma_t *hudx)
{
	size_t w, r, cnt;

	// Operate on temporary values in case they change in between
	w = hudx->wAbsTX;
	r = hudx->rAbsTX;

	cnt = w-r;

#ifndef UART_DMA_TX_BUFFER_LEN_POW_2
	if (r > w) cnt -= UART_DMA_TX_BUFFER_NON_USED_IDX_SPACE;
#endif

	return cnt;
}

/**
 * \brief           Get free space available in RX buffer in bytes
 * \param[in]       hudx: Pointer to UART DMA structure
 * \return          Space available in bytes
 */
size_t uart_dma_free_rx(uart_dma_t *hudx)
{
	// Determine free space
	return UART_DMA_RX_BUFFER_LEN - uart_dma_cnt_rx(hudx);
}

/**
 * \brief           Get free space available in TX buffer in bytes
 * \param[in]       hudx: Pointer to UART DMA structure
 * \return          Space available in bytes
 */
size_t uart_dma_free_tx(uart_dma_t *hudx)
{
	// Determine free space
	return UART_DMA_TX_BUFFER_LEN - uart_dma_cnt_tx(hudx);
}

/**
 * \brief           Get number of bytes in RX buffer relative to reference
 *
 * 					Returns the number of bytes available starting from given reference to current write index.
 * 					In case the given reference results in a number of bytes readable greater than the buffer size,
 * 					zero is returned. This means the given reference is invalid as such it points either to
 * 					a region below the current write index minus the buffer size or an index before the write index
 * 					(taking taking the wrap around into consideration).
 *
 * \param[in]       hudx: Pointer to UART DMA structure
 * \param[in]       offset: Number of bytes to ignore before determining cnt
 * \param[in]      	ref: Reference to start from (absolute buffer index)
 * \return          Number of bytes in TX buffer
 */
size_t uart_dma_cnt_rel_to_ref_rx(uart_dma_t *hudx, size_t offset, size_t ref)
{
	// Determine number of bytes readable
	size_t w = hudx->wAbsRX;
	size_t cnt = w - ref;

#ifndef UART_DMA_RX_BUFFER_LEN_POW_2
	if (ref > w) cnt -= UART_DMA_RX_BUFFER_NON_USED_IDX_SPACE;
#endif

	// Check if number of bytes is not bigger than buffer size - otherwise the given reference was before the write index and is thus invalid
	if (cnt > UART_DMA_RX_BUFFER_LEN) return 0;

	return cnt;
}

/**
 * \brief           Get number of bytes in TX buffer relative to reference
 *
 * 					Returns the number of bytes available starting from given reference to current write index.
 * 					In case the given reference results in a number of bytes readable greater than the buffer size,
 * 					zero is returned. This means the given reference is invalid as such it points either to
 * 					a region below the current write index minus the buffer size or an index before the write index
 * 					(taking taking the wrap around into consideration).
 *
 * \param[in]       hudx: Pointer to UART DMA structure
 * \param[in]       offset: Number of bytes to ignore before determining cnt
 * \param[in]      	ref: Reference to start from (absolute buffer index)
 * \return          Number of bytes in TX buffer
 */
size_t uart_dma_cnt_rel_to_ref_tx(uart_dma_t *hudx, size_t offset, size_t ref)
{
	// Determine number of bytes readable
	size_t w = hudx->wAbsTX;
	size_t cnt = w - ref - offset;
#ifndef UART_DMA_TX_BUFFER_LEN_POW_2
	if (ref > w) cnt -= UART_DMA_TX_BUFFER_NON_USED_IDX_SPACE;
#endif

	// Check if number of bytes is not bigger than buffer size - otherwise the given reference was before the write index and is thus invalid
	if (cnt > UART_DMA_TX_BUFFER_LEN) return 0;

	return cnt;
}

/**
 * \brief           Read from buffer but do not change read pointer
 * \param[in]       offset: Number of bytes to ignore before reading data
 * \param[out]      data: Pointer to data to be written to
 * \param[in]       n: Number of bytes to peek
 * \param[in]       rRel: Relative read pointer
 * \param[in]       pBuff: Pointer to buffer (RX or TX)
 * \param[in]       buffLen: Length of buffer
 * \param[in]       cnt: Number of bytes in buffer
 * \return          Number of bytes written to data
 */
static size_t uart_dma_peek(size_t offset, void* data, size_t n, size_t rRel, uint8_t * pBuff, size_t buffLen, size_t cnt)
{
	uint8_t *d = data;

	// Skip beginning of buffer
	if (cnt == 0 || offset >= cnt) {
		return 0;
	}

	// Check if we can read something after skip
	cnt -= offset;
	if (cnt < n) {
		if (cnt == 0) {
			return 0;
		}
		n = cnt;
	}

	// Read data
	if(rRel + n <= buffLen) 	// Linear mode only
	{
		// Read data
		memcpy(d, &pBuff[rRel], n);
	}
	else 	// Wrap around mode
	{
		size_t nLin = buffLen - rRel;

		// Read data from linear part of buffer
		memcpy(d, &pBuff[rRel], nLin);

		// Read data wrapped around
		memcpy(&d[nLin], pBuff, n - nLin);
	}
	return n;
}

/**
 * \brief           Read from RX buffer but do not change read pointer
 * \param[in]       hudx: Pointer to UART DMA structure
 * \param[in]       offset: Number of bytes to ignore before reading data
 * \param[out]      data: Pointer to data to be written to
 * \param[in]       n: Number of bytes to peek
 * \return          Number of bytes written to data
 */
size_t uart_dma_peek_rx(uart_dma_t *hudx, size_t offset, void* data, size_t n)
{
	size_t cnt = uart_dma_cnt_rx(hudx);
	size_t rRel = hudx->rAbsRX;
	rRel = get_relative_pointer_rx(rRel, offset);

	return uart_dma_peek(offset, data, n, rRel, hudx->huart->pRxBuffPtr, UART_DMA_RX_BUFFER_LEN, cnt);
}

/**
 * \brief           Read from TX buffer but do not change read pointer
 * \param[in]       hudx: Pointer to UART DMA structure
 * \param[in]       offset: Number of bytes to ignore before reading data
 * \param[out]      data: Pointer to data to be written to
 * \param[in]       n: Number of bytes to peek
 * \return          Number of bytes written to data
 */
size_t uart_dma_peek_tx(uart_dma_t *hudx, size_t offset, void* data, size_t n)
{
	size_t cnt = uart_dma_cnt_tx(hudx);
	size_t rRel = hudx->rAbsTX;
	rRel = get_relative_pointer_tx(rRel, offset);

	return uart_dma_peek(offset, data, n, rRel, hudx->pBuffTX, UART_DMA_TX_BUFFER_LEN, cnt);
}

/**
 * \brief           Read single byte from buffer but do not change read pointer
 * \param[in]       offset: Number of bytes to ignore before reading data
 * \param[out]      data: Pointer to copy byte to
 * \param[in]       rRel: Relative read pointer
 * \param[in]       pBuff: Pointer to buffer (RX or TX)
 * \param[in]       cnt: Number of bytes in buffer
 * \return          Number of bytes written
 */
static size_t uart_dma_peek_byte(size_t offset, uint8_t *data, size_t rRel, uint8_t * pBuff, size_t cnt)
{

	// Skip beginning of buffer
	if (offset >= cnt) return 0;

	// Copy byte
	*data = pBuff[rRel];

	return 1;
}

/**
 * \brief           Read single byte from buffer RX but do not change read pointer
 * \param[in]       hudx: Pointer to UART DMA structure
 * \param[in]       offset: Number of bytes to ignore before reading data
 * \param[out]      data: Pointer to copy byte to
 * \return          Number of bytes written (1 or 0)
 */
size_t uart_dma_peek_byte_rx(uart_dma_t *hudx, size_t offset, uint8_t *data)
{
	size_t rRel;

	// Calculate maximum number of bytes we can read
	size_t cnt = uart_dma_cnt_rx(hudx);

	// Work on local variables
	rRel = hudx->rAbsRX;

	// Get relative read pointer
	rRel = get_relative_pointer_rx(rRel, offset);

	return uart_dma_peek_byte(offset, data, rRel, hudx->huart->pRxBuffPtr, cnt);
}

/**
 * \brief           Read single byte from buffer TX but do not change read pointer
 * \param[in]       hudx: Pointer to UART DMA structure
 * \param[in]       offset: Number of bytes to ignore before reading data
 * \param[out]      data: Pointer to copy byte to
 * \return          Number of bytes written (1 or 0)
 */
size_t uart_dma_peek_byte_tx(uart_dma_t *hudx, size_t offset, uint8_t *data)
{
	size_t rRel;

	// Calculate maximum number of bytes we can read
	size_t cnt = uart_dma_cnt_tx(hudx);

	// Work on local variables
	rRel = hudx->rAbsTX;

	// Get relative read pointer
	rRel = get_relative_pointer_tx(rRel, offset);

	return uart_dma_peek_byte(offset, data, rRel, hudx->pBuffTX, cnt);
}

/**
 * \brief           Read from RX buffer relative to given reference
 *
 * 					Read byte in RX buffer relative to given reference (absolute index). No read or write pointers are modified.
 * 					Peek starts at given reference and peeks until current write pointer. The maximum number of bytes which may be peeked
 * 					is the size of the buffer UART_DMA_RX_BUFFER_LEN. If the given reference results in a number of bytes to be peeked
 * 					greater than UART_DMA_RX_BUFFER_LEN, nothing will be copied.
 *
 * \param[in]       hudx: Pointer to UART DMA structure
 * \param[in]       offset: Number of bytes to ignore before reading data
 * \param[in]       ref: Reference to start from (absolute index)
 * \param[out]      data: Pointer to data to be written to
 * \param[in]       n: Number of bytes to peek
 * \return          Number of bytes written to data
 */
size_t uart_dma_peek_by_ref_rx(uart_dma_t *hudx, size_t offset, size_t ref, void* data, size_t n)
{
	// Determine number of bytes readable
	size_t cnt = uart_dma_cnt_rel_to_ref_rx(hudx, offset, ref);

	size_t rRel = get_relative_pointer_rx(ref, offset);

	return uart_dma_peek(offset, data, n, rRel, hudx->huart->pRxBuffPtr, UART_DMA_RX_BUFFER_LEN, cnt);
}

/**
 * \brief           Read from TX buffer relative to given reference
 *
 * 					Read byte in TX buffer relative to given reference (absolute index). No read or write pointers are modified.
 * 					Peek starts at given reference and peeks until current write pointer. The maximum number of bytes which may be peeked
 * 					is the size of the buffer UART_DMA_TX_BUFFER_LEN. If the given reference results in a number of bytes to be peeked
 * 					greater than UART_DMA_TX_BUFFER_LEN, nothing will be copied.
 *
 * \param[in]       hudx: Pointer to UART DMA structure
 * \param[in]       offset: Number of bytes to ignore before reading data
 * \param[in]       ref: Reference to start from (absolute index)
 * \param[out]      data: Pointer to data to be written to
 * \param[in]       n: Number of bytes to peek
 * \return          Number of bytes written to data
 */
size_t uart_dma_peek_by_ref_tx(uart_dma_t *hudx, size_t offset, size_t ref, void* data, size_t n)
{
	// Determine number of bytes readable
	size_t cnt = uart_dma_cnt_rel_to_ref_tx(hudx, offset, ref);

	size_t rRel = get_relative_pointer_tx(ref, offset);

	return uart_dma_peek(offset, data, n, rRel, hudx->pBuffTX, UART_DMA_TX_BUFFER_LEN, cnt);
}

/**
 * \brief           Read byte from RX buffer relative to given reference
 *
 * 					Read byte in RX buffer relative to given reference (absolute index). No read or write pointers are modified.
 *
 * \param[in]       hudx: Pointer to handle
 * \param[in]       offset: Number of bytes to advance before start writing
 * \param[in]       ref: Reference to start from (absolute index)
 */
uint8_t uart_dma_peek_byte_by_ref_rx(uart_dma_t *hudx, size_t offset, size_t ref)
{
	// Use local variables
	size_t rRelTmp = get_relative_pointer_rx(ref, offset);

	// Get byte
	return hudx->huart->pRxBuffPtr[rRelTmp];
}

/**
 * \brief           Read byte from TX buffer relative to given reference
 *
 * 					Read byte in TX buffer relative to given reference (absolute index). No read or write pointers are modified.
 *
 * \param[in]       hudx: Pointer to handle
 * \param[in]       offset: Number of bytes to advance before start writing
 * \param[in]       ref: Reference to start from (absolute index)
 */
uint8_t uart_dma_peek_byte_by_ref_tx(uart_dma_t *hudx, size_t offset, size_t ref)
{
	// Use local variables
	size_t rRelTmp = get_relative_pointer_tx(ref, offset);

	// Set byte
	return hudx->pBuffTX[rRelTmp];
}

/**
 * \brief           Read data from RX buffer
 *
 * 					Read bytes are flushed from buffer after read.
 *
 * \param[in]       hudx: Pointer to UART DMA structure
 * \param[in]       offset: Number of bytes to ignore before reading data
 * \param[out]      data: Pointer to copy byte to
 * \param[in]       n: Number of bytes we want to read
 * \return          Number of bytes written to data
 */
size_t uart_dma_read_rx(uart_dma_t *hudx, size_t offset, void* data, size_t n)
{
	size_t nRead = uart_dma_peek_rx(hudx, offset, data, n);

	// Advance read pointer
	hudx->rAbsRX = advance_pointer_rx(hudx->rAbsRX, nRead);

	return nRead;
}

/**
 * \brief           Read data from TX buffer
 *
 * 					Read bytes are flushed from buffer after read.
 *
 * \param[in]       hudx: Pointer to UART DMA structure
 * \param[in]       offset: Number of bytes to ignore before reading data
 * \param[out]      data: Pointer to copy byte to
 * \param[in]       n: Number of bytes we want to read
 * \return          Number of bytes written to data
 */
size_t uart_dma_read_tx(uart_dma_t *hudx, size_t offset, void* data, size_t n)
{
	size_t nRead = uart_dma_peek_tx(hudx, offset, data, n);

	// Advance read pointer
	hudx->rAbsTX = advance_pointer_tx(hudx->rAbsTX, nRead);

	return nRead;
}

/**
 * \brief           Write data to buffer given an offset relative to read pointer - intended for in buffer copies
 *
 * 	Write data into buffer given an offset relative to read pointer. This function is save for in buffer copies as
 * 	copies are conducted by use of memmove(). The read and write pointers are not changed.
 *
 * \param[in]       offset: Offset relative to read pointer where data is to be written
 * \param[in]      	data: Pointer to data array to be copied from
 * \param[in]      	len: # of array elements to copy
 * \param[in]       wRelTmp: Relative write pointer
 * \param[in]       pBuff: Pointer to buffer structure
 * \param[in]       buffLen: Length of buffer
 * \return          Number of bytes actually written
 */
static size_t uart_dma_modify(size_t offset, void* data, size_t len, size_t wRelTmp, uint8_t * pBuff, size_t buffLen)
{
	const uint8_t* d = data;

	if (len < buffLen)
	{
		// Check how to copy
		if(wRelTmp + len <= buffLen) 	// Linear mode only
		{
			// Copy data - we do not use memcpy since this is not save for overlapping array copies
			memmove((void *) &(pBuff[wRelTmp]), (void *) d, len);
		}
		else 	// Wrap around mode
		{
			size_t nLin = buffLen - wRelTmp;

			// Write data to linear part of buffer
			memmove((void *) &(pBuff[wRelTmp]), (void *) d, nLin);

			// Write data wrapped around
			memmove((void *) pBuff, (void *) &d[nLin], nLin);
		}

		return len;                      // Return number of elements stored in memory
	}
	else
	{
		return 0;
	}
}

/**
 * \brief           Write data to RX buffer given an offset relative to read pointer - intended for in buffer copies
 *
 * 	Write data into buffer given an offset relative to read pointer. This function is save for in buffer copies as
 * 	copies are conducted by use of memmove(). The read and write pointers are not changed.
 *
 * \param[in]       hudx: Pointer to UART DMA structure
 * \param[in]       offset: Offset relative to read pointer where data is to be written
 * \param[in]      	data: Pointer to data array to be copied from
 * \param[in]      	len: # of array elements to copy
 * \return          Number of bytes actually written
 */
size_t uart_dma_modify_rx(uart_dma_t *hudx, size_t offset, void* data, size_t len)
{
	size_t wRelTmp;

	// Use local variables
	wRelTmp = hudx->rAbsRX;

	// Calculate relative index to start to write to
	wRelTmp = get_relative_pointer_rx(wRelTmp, offset);

	return uart_dma_modify(offset, data, len, wRelTmp, hudx->huart->pRxBuffPtr, UART_DMA_RX_BUFFER_LEN);
}

/**
 * \brief           Write data to TX buffer given an offset relative to read pointer - intended for in buffer copies
 *
 * 	Write data into buffer given an offset relative to read pointer. This function is save for in buffer copies as
 * 	copies are conducted by use of memmove(). The read and write pointers are not changed.
 *
 * \param[in]       hudx: Pointer to UART DMA structure
 * \param[in]       offset: Offset relative to read pointer where data is to be written
 * \param[in]      	data: Pointer to data array to be copied from
 * \param[in]      	len: # of array elements to copy
 * \return          Number of bytes actually written
 */
size_t uart_dma_modify_tx(uart_dma_t *hudx, size_t offset, void* data, size_t len)
{
	size_t wRelTmp;

	// Use local variables
	wRelTmp = hudx->rAbsTX;

	// Calculate relative index to start to write to
	wRelTmp = get_relative_pointer_tx(wRelTmp, offset);

	return uart_dma_modify(offset, data, len, wRelTmp, hudx->pBuffTX, UART_DMA_TX_BUFFER_LEN);
}

/**
 * \brief           Write data to buffer - as much as possible - NOT intended for in buffer copies, use uart_dma_move() instead
 *
 * 	Write data to buffer as much as possible. In case there is not sufficient space in buffer, this functions copies as much as
 * 	possible and return the number of copied bytes.
 *
 * \param[in]      	data: Pointer to data array to be copied from
 * \param[in]      	len: Length of data to be copied
 * \param[in]       wRelTmp: Relative write pointer
 * \param[in]       pBuff: Pointer to buffer structure
 * \param[in]       buffLen: Length of buffer
 * \return          Number of bytes actually written
 */
static void uart_dma_write(void * data, size_t len, size_t wRelTmp, uint8_t * pBuff, size_t buffLen)
{
	const uint8_t* d = data;

	// Check how to copy
	if(wRelTmp + len <= buffLen) 	// Linear mode only
	{
		// Copy data - we do not use memcpy since this is not save for overlapping array copies
		memcpy((void *) &(pBuff[wRelTmp]), (void *) d, len);
	}
	else 	// Wrap around mode
	{
		size_t nLin = buffLen - wRelTmp;

		// Write data to linear part of buffer
		memcpy((void *) &(pBuff[wRelTmp]), (void *) d, nLin);

		// Write data wrapped around
		memcpy((void *) pBuff, (void *) &d[nLin], len - nLin);
	}
}

/**
 * \brief           Write data to RX buffer - as much as possible - NOT intended for in buffer copies, use uart_dma_move() instead
 *
 *  Write data to buffer as much as possible. In case there is not sufficient space in buffer, this functions copies as much as
 * 	possible and returns the number of copied bytes.
 *
 * \param[in]       hudx: Pointer to UART DMA structure
 * \param[in]      	data: Pointer to data array to be copied from
 * \param[in]      	len: # of array elements to copy
 * \return          Number of bytes actually written
 */
size_t uart_dma_write_1_rx(uart_dma_t *hudx, void* data, size_t len)
{
	size_t wRelTmp, cnt, wAbsRX;

	// Use local variables
	wAbsRX = hudx->wAbsRX;
	wRelTmp = get_relative_pointer_rx(wAbsRX, 0);

	cnt = uart_dma_cnt_rx(hudx);

	// Determine free space
	size_t free = UART_DMA_RX_BUFFER_LEN - cnt;

	// Clip length if needed
	if (len > free) len = free;

	// Write data
	uart_dma_write(data, len, wRelTmp, hudx->huart->pRxBuffPtr, UART_DMA_RX_BUFFER_LEN);

	// Advance pointer
	hudx->wAbsRX = advance_pointer_rx(wAbsRX, len);

	// Return number of bytes written
	return len;
}

/**
 * \brief           Write data to TX buffer - as much as possible - NOT intended for in buffer copies, use uart_dma_move() instead
 *
 *  Write data to buffer as much as possible. In case there is not sufficient space in buffer, this functions copies as much as
 * 	possible and return the number of copied bytes.
 *
 * \param[in]       hudx: Pointer to UART DMA structure
 * \param[in]      	data: Pointer to data array to be copied from
 * \param[in]      	len: # of array elements to copy
 * \return          Number of bytes actually written
 */
size_t uart_dma_write_1_tx(uart_dma_t *hudx, void* data, size_t len)
{
	size_t wRelTmp, cnt, wAbsTX;

	// Use local variables
	wAbsTX = hudx->wAbsTX;
	wRelTmp = get_relative_pointer_tx(wAbsTX, 0);

	cnt = uart_dma_cnt_tx(hudx);

	// Determine free space
	size_t free = UART_DMA_TX_BUFFER_LEN - cnt;

	// Clip length if needed
	if (len > free) len = free;

	// Write data
	uart_dma_write(data, len, wRelTmp, hudx->pBuffTX, UART_DMA_TX_BUFFER_LEN);

	// Advance pointer
	hudx->wAbsTX = advance_pointer_tx(wAbsTX, len);

	// Save number of newly added bytes
	hudx->nNewBytes += len;

	// Return number of bytes written
	return len;
}

/**
 * \brief           Write data to RX buffer - denies copy if not sufficient space available - NOT intended for in buffer copies, use uart_dma_move() instead
 *
 *  Write data to buffer if enough space is available. In case there is not sufficient space in buffer,
 *  this functions copies nothing and returns UART_DMA_RX_ERROR_OVERFLOW.
 *
 * \param[in]       hudx: Pointer to UART DMA structure
 * \param[in]      	data: Pointer to data array to be copied from
 * \param[in]      	len: # of array elements to copy
 * \return          UART_DMA_OK on success or UART_DMA_RX_ERROR_OVERFLOW if no sufficient space is available
 */
uart_dma_status_t uart_dma_write_2_rx(uart_dma_t *hudx, void* data, size_t len)
{
	size_t wRelTmp, cnt, wAbsRX;

	cnt = uart_dma_cnt_rx(hudx);

	// Determine free space
	size_t free = UART_DMA_RX_BUFFER_LEN - cnt;

	// Clip length if needed
	if (len > free) return UART_DMA_RX_ERROR_OVERFLOW;

	// Use local variables
	wAbsRX = hudx->wAbsRX;
	wRelTmp = get_relative_pointer_rx(wAbsRX, 0);

	// Write data
	uart_dma_write(data, len, wRelTmp, hudx->huart->pRxBuffPtr, UART_DMA_RX_BUFFER_LEN);

	// Advance pointer
	hudx->wAbsRX = advance_pointer_rx(wAbsRX, len);

	return UART_DMA_OK;
}

/**
 * \brief           Write data to TX buffer - denies copy if not sufficient space available - NOT intended for in buffer copies, use uart_dma_move() instead
 *
 *  Write data to buffer if enough space is available. In case there is not sufficient space in buffer,
 *  this functions copies nothing and returns UART_DMA_TX_ERROR_OVERFLOW.
 *
 * \param[in]       hudx: Pointer to UART DMA structure
 * \param[in]      	data: Pointer to data array to be copied from
 * \param[in]      	len: # of array elements to copy
 * \return          UART_DMA_OK on success or UART_DMA_TX_ERROR_OVERFLOW if no sufficient space is available
 */
uart_dma_status_t uart_dma_write_2_tx(uart_dma_t *hudx, void* data, size_t len)
{
	size_t wRelTmp, cnt, wAbsTX;

	cnt = uart_dma_cnt_tx(hudx);

	// Determine free space
	size_t free = UART_DMA_TX_BUFFER_LEN - cnt;

	// Clip length if needed
	if (len > free) return UART_DMA_TX_ERROR_OVERFLOW;

	// Use local variables
	wAbsTX = hudx->wAbsTX;
	wRelTmp = get_relative_pointer_tx(wAbsTX, 0);

	// Write data
	uart_dma_write(data, len, wRelTmp, hudx->pBuffTX, UART_DMA_TX_BUFFER_LEN);

	// Advance pointer
	hudx->wAbsTX = advance_pointer_tx(wAbsTX, len);

	// Save number of newly added bytes
	hudx->nNewBytes += len;

	return UART_DMA_OK;
}

/**
 * \brief           Write single byte into TX buffer
 *
 * \param[in]       hudx: Pointer to UART DMA structure
 * \param[in]      	data: Byte to copy into buffer
 * \return          UART_DMA_OK on success or UART_DMA_TX_ERROR_OVERFLOW if no sufficient space is available
 */
uart_dma_status_t uart_dma_put_tx(uart_dma_t *hudx, uint8_t data)
{
	size_t wRelTmp, cnt, wAbsTX;

	cnt = uart_dma_cnt_tx(hudx);

	// Check if space is available
	if (cnt == UART_DMA_TX_BUFFER_LEN) return UART_DMA_TX_ERROR_OVERFLOW;

	// Use local variables
	wAbsTX = hudx->wAbsTX;
	wRelTmp = get_relative_pointer_tx(wAbsTX, 0);

	// Write data
	hudx->pBuffTX[wRelTmp] = data;

	// Advance pointer
	hudx->wAbsTX = advance_pointer_tx(wAbsTX, 1);

	// Save number of newly added bytes for sending
	hudx->nNewBytes += 1;

	return UART_DMA_OK;
}

/**
 * \brief           Write single byte into RX buffer
 *
 * \param[in]       hudx: Pointer to UART DMA structure
 * \param[in]      	data: Byte to copy into buffer
 * \return          UART_DMA_OK on success or UART_DMA_RX_ERROR_OVERFLOW if no sufficient space is available
 */
uart_dma_status_t uart_dma_put_rx(uart_dma_t *hudx, uint8_t data)
{
	size_t wRelTmp, cnt, wAbsRX;

	cnt = uart_dma_cnt_rx(hudx);

	// Check if space is available
	if (cnt == UART_DMA_RX_BUFFER_LEN) return UART_DMA_RX_ERROR_OVERFLOW;

	// Use local variables
	wAbsRX = hudx->wAbsRX;
	wRelTmp = get_relative_pointer_rx(wAbsRX, 0);

	// Write data
	hudx->huart->pRxBuffPtr[wRelTmp] = data;

	// Advance pointer
	hudx->wAbsRX = advance_pointer_rx(wAbsRX, 1);

	return UART_DMA_OK;
}

/**
 * \brief           Write single byte into RX buffer given an offset relative to read pointer - intended for in buffer copies
 *
 * 					Write a single byte into buffer. The read and write pointers are not changed.
 *
 * \param[in]       hudx: Pointer to UART DMA structure
 * \param[in]       offset: Offset relative to read pointer where data is to be written
 * \param[in]      	data: Byte to insert
 */
void uart_dma_modify_byte_rx(uart_dma_t *hudx, size_t offset, uint8_t data)
{
	// Use local variables
	size_t wRelTmp = hudx->rAbsRX;

	// Calculate relative index to start to write to
	wRelTmp = get_relative_pointer_rx(wRelTmp, offset);

	// Save byte
	hudx->huart->pRxBuffPtr[wRelTmp] = data;
}

/**
 * \brief           Write single byte into TX buffer given an offset relative to read pointer - intended for in buffer copies
 *
 * 					Write a single byte into buffer. The read and write pointers are not changed.
 *
 * \param[in]       hudx: Pointer to UART DMA structure
 * \param[in]       offset: Offset relative to read pointer where data is to be written
 * \param[in]      	data: Byte to insert
 */
void uart_dma_modify_byte_tx(uart_dma_t *hudx, size_t offset, uint8_t data)
{
	// Use local variables
	size_t wRelTmp = hudx->rAbsTX;

	// Calculate relative index to start to write to
	wRelTmp = get_relative_pointer_tx(wRelTmp, offset);

	// Save byte
	hudx->pBuffTX[wRelTmp] = data;
}

/**
 * \brief           Get linear address for RX buffer for fast read
 * \param[in]       hudx: Pointer to handle
 * \param[in]       offset: Number of bytes to ignore before getting address
 * \return          Linear buffer start address
 */
void * uart_dma_get_linear_block_address_rx(uart_dma_t *hudx, size_t offset)
{
	// Work on local variables
	size_t rRel = hudx->rAbsRX;

	// Get relative read pointer
	rRel = get_relative_pointer_rx(rRel, offset);

	return &hudx->huart->pRxBuffPtr[rRel];
}

/**
 * \brief           Get linear address for TX buffer for fast read
 * \param[in]       hudx: Pointer to handle
 * \param[in]       offset: Number of bytes to ignore before getting address
 * \return          Linear buffer start address
 */
void * uart_dma_get_linear_block_address_tx(uart_dma_t *hudx, size_t offset)
{
	// Work on local variables
	size_t rRel = hudx->rAbsTX;

	// Get relative read pointer
	rRel = get_relative_pointer_tx(rRel, offset);

	return &hudx->pBuffTX[rRel];
}

/**
 * \brief           Get linear address relative to given reference for RX buffer for fast read
 * \param[in]       hudx: Pointer to handle
 * \param[in]       offset: Number of bytes to ignore before getting address
 * \param[in]       ref: Reference to start from
 * \return          Linear buffer start address
 */
void * uart_dma_get_linear_block_address_by_ref_rx(uart_dma_t *hudx, size_t offset, size_t ref)
{
	// Get relative read pointer
	size_t rRel = get_relative_pointer_rx(ref, offset);

	return &hudx->huart->pRxBuffPtr[rRel];
}

/**
 * \brief           Get linear address relative to given reference for TX buffer for fast read
 * \param[in]       hudx: Pointer to handle
 * \param[in]       offset: Number of bytes to ignore before getting address
 * \param[in]       ref: Reference to start from
 * \return          Linear buffer start address
 */
void * uart_dma_get_linear_block_address_by_ref_tx(uart_dma_t *hudx, size_t offset, size_t ref)
{
	// Get relative read pointer
	size_t rRel = get_relative_pointer_tx(ref, offset);

	return &hudx->pBuffTX[rRel];
}

/**
 * \brief           Get length of linear block address in RX buffer before wrap around
 * \param[in]       hudx: Pointer to handle
 * \param[in]       offset: Number of bytes to ignore before getting length
 * \return          Linear buffer size in units of bytes
 */
size_t uart_dma_get_linear_block_length_rx(uart_dma_t *hudx, size_t offset)
{
	size_t wRel, rRel, len;

	// Operate on temporary values in case they change in between
	wRel = hudx->wAbsRX;
	rRel = hudx->rAbsRX;

	// Get relative pointers
	wRel = get_relative_pointer_rx(wRel, 0);
	rRel = get_relative_pointer_rx(rRel, offset);

	if (wRel >= rRel) {
		len = wRel - rRel;
	}
	else
	{
		len = UART_DMA_RX_BUFFER_LEN - rRel;
	}
	return len;
}

/**
 * \brief           Get length of linear block address in TX buffer before wrap around
 * \param[in]       hudx: Pointer to handle
 * \param[in]       offset: Number of bytes to ignore before getting length
 * \return          Linear buffer size in units of bytes
 */
size_t uart_dma_get_linear_block_length_tx(uart_dma_t *hudx, size_t offset)
{
	size_t wRel, rRel, len;

	// Operate on temporary values in case they change in between
	wRel = hudx->wAbsTX;
	rRel = hudx->rAbsTX;

	// Get relative pointers
	wRel = get_relative_pointer_tx(wRel, 0);
	rRel = get_relative_pointer_tx(rRel, offset);

	if (wRel >= rRel) {
		len = wRel - rRel;
	}
	else
	{
		len = UART_DMA_TX_BUFFER_LEN - rRel;
	}
	return len;
}

/**
 * \brief           Get length of linear block address by reference in RX buffer before wrap around
 * \param[in]       hudx: Pointer to handle
 * \param[in]       offset: Number of bytes to ignore before getting length
 * \param[in]       ref: Reference to start from
 * \return          Linear buffer size in units of bytes
 */
size_t uart_dma_get_linear_block_length_by_ref_rx(uart_dma_t *hudx, size_t offset, size_t ref)
{
	size_t wRel, rRel, len;

	// Operate on temporary values in case they change in between
	wRel = hudx->wAbsRX;

	// Get relative pointers
	wRel = get_relative_pointer_rx(wRel, 0);
	rRel = get_relative_pointer_rx(ref, offset);

	if (wRel >= rRel) {
		len = wRel - rRel;
	}
	else
	{
		len = UART_DMA_RX_BUFFER_LEN - rRel;
	}
	return len;
}

/**
 * \brief           Get length of linear block address by reference in TX buffer before wrap around
 * \param[in]       hudx: Pointer to handle
 * \param[in]       offset: Number of bytes to ignore before getting length
 * \param[in]       ref: Reference to start from
 * \return          Linear buffer size in units of bytes
 */
size_t uart_dma_get_linear_block_length_by_ref_tx(uart_dma_t *hudx, size_t offset, size_t ref)
{
	size_t wRel, rRel, len;

	// Operate on temporary values in case they change in between
	wRel = hudx->wAbsTX;

	// Get relative pointers
	wRel = get_relative_pointer_tx(wRel, 0);
	rRel = get_relative_pointer_tx(ref, offset);

	if (wRel >= rRel) {
		len = wRel - rRel;
	}
	else
	{
		len = UART_DMA_TX_BUFFER_LEN - rRel;
	}
	return len;
}

/**
 * \brief Get linear read info
 *
 * Returns the lengths corresponding to given pointers which may be read in a linear manner.
 * This is of major interest for DMA transmissions. If returned lengths are zero the
 * corresponding pointers are invalid.
 *
 * \param[in]       *len1: Length of first linear part. If zero corresponding pointer p1 is invalid
 * \param[in]       *len2: Length of second linear part. If zero corresponding pointer p2 is invalid
 * \param[in]       **p1: Pointer to start read of first part
 * \param[in]       **p2: Pointer to start read of second part
 * \param[in]       wRel: Relative write pointer
 * \param[in]       rRel: Relative read pointer
 * \param[in]       pBuff: Pointer to buffer
 * \param[in]       buffLen: Length of buffer
 * \param[in]       cnt: Number of bytes in buffer
 */
static void uart_dma_get_linear_read_info(size_t *len1, size_t * len2, void ** p1, void ** p2, size_t wRel, size_t rRel, uint8_t * pBuff, size_t buffLen, size_t cnt)
{
	size_t len;

	if (wRel >= rRel) {
		len = wRel - rRel;
	}
	else
	{
		len = buffLen - rRel;
	}

	*len1 = len;
	*p1 = &pBuff[rRel];

	// Check if there is a wrap around necessary
	if(cnt > *len1)
	{
		*len2 = cnt - *len1;
		*p2 = pBuff;
	}
	else
	{
		*len2 = 0;
	}
}

/**
 * \brief Get linear read info of RX buffer
 *
 * Returns the lengths corresponding to given pointers which may be read in a linear manner.
 * This is of major interest for DMA transmissions. If returned lengths are zero the
 * corresponding pointers are invalid.
 *
 * \param[in]       hudx: Pointer to handle
 * \param[in]       offset: Number of bytes to ignore before start reading
 * \param[in]       *len1: Length of first linear part. If zero corresponding pointer p1 is invalid
 * \param[in]       *len2: Length of second linear part. If zero corresponding pointer p2 is invalid
 * \param[in]       **p1: Pointer to start read of first part
 * \param[in]       **p2: Pointer to start read of second part
 */
void uart_dma_get_linear_read_info_rx(uart_dma_t *hudx, size_t offset, size_t *len1, size_t * len2, void ** p1, void ** p2)
{
	size_t wRel, rRel, cnt;

	// Operate on temporary values in case they change in between
	wRel = hudx->wAbsRX;
	rRel = hudx->rAbsRX;

	// Get relative pointers
	wRel = get_relative_pointer_rx(wRel, 0);
	rRel = get_relative_pointer_rx(rRel, offset);

	// Check if there is a wrap around necessary
	cnt = uart_dma_cnt_rx(hudx);

	uart_dma_get_linear_read_info(len1, len2, p1, p2, wRel, rRel, hudx->huart->pRxBuffPtr, UART_DMA_RX_BUFFER_LEN, cnt);
}

/**
 * \brief Get linear read info of TX buffer
 *
 * Returns the lengths corresponding to given pointers which may be read in a linear manner.
 * This is of major interest for DMA transmissions. If returned lengths are zero the
 * corresponding pointers are invalid.
 *
 * \param[in]       hudx: Pointer to handle
 * \param[in]       offset: Number of bytes to ignore before start reading
 * \param[in]       *len1: Length of first linear part. If zero corresponding pointer p1 is invalid
 * \param[in]       *len2: Length of second linear part. If zero corresponding pointer p2 is invalid
 * \param[in]       **p1: Pointer to start read of first part
 * \param[in]       **p2: Pointer to start read of second part
 */
void uart_dma_get_linear_read_info_tx(uart_dma_t *hudx, size_t offset, size_t *len1, size_t * len2, void ** p1, void ** p2)
{
	size_t wRel, rRel, cnt;

	// Operate on temporary values in case they change in between
	wRel = hudx->wAbsTX;
	rRel = hudx->rAbsTX;

	// Get relative pointers
	wRel = get_relative_pointer_tx(wRel, 0);
	rRel = get_relative_pointer_tx(rRel, offset);

	// Check if there is a wrap around necessary
	cnt = uart_dma_cnt_tx(hudx);

	uart_dma_get_linear_read_info(len1, len2, p1, p2, wRel, rRel, hudx->pBuffTX, UART_DMA_TX_BUFFER_LEN, cnt);
}

/**
 * \brief           Reserve space in RX buffer for later insertions
 *
 * 					Reserves space in buffer by advancing write pointer
 * 					and returning reference address of reserved space. Useful
 * 					if some bytes have to be written some point later than
 * 					other writes e.g. address or length information of frame
 * 					which is written into the buffer.
 *
 * \param[in]       hudx: Pointer to handle
 * \param[in]       len: Number of bytes to reserve
 * \param[out]      pRef: Pointer to which reference of reserved space is saved to. Needed for later insertions e.g. uart_dma_set_by_ref()
 * \return          UART_DMA_OK on success or UART_DMA_RX_ERROR_OVERFLOW if no sufficient space is available
 */
uart_dma_status_t uart_dma_reserve_space_rx(uart_dma_t *hudx, size_t len, size_t * pRef)
{
	size_t cnt;

	cnt = uart_dma_cnt_rx(hudx);

	// Determine free space
	size_t free = UART_DMA_RX_BUFFER_LEN - cnt;

	// Clip length if needed
	if (len > free) return UART_DMA_RX_ERROR_OVERFLOW;

	// Save reference
	*pRef = hudx->wAbsRX;

	// Advance pointer
	hudx->wAbsRX = advance_pointer_rx(hudx->wAbsRX, len);

	return UART_DMA_OK;
}

/**
 * \brief           Reserve space in TX buffer for later insertions
 *
 * 					Reserves space in buffer by advancing write pointer
 * 					and returning reference address of reserved space. Useful
 * 					if some bytes have to be written some point later than
 * 					other writes e.g. address or length information of frame
 * 					which is written into the buffer.
 *
 * \param[in]       hudx: Pointer to handle
 * \param[in]       len: Number of bytes to reserve
 * \param[out]      pRef: Pointer to which reference of reserved space is saved to. Needed for later insertions e.g. uart_dma_set_by_ref()
 * \return          UART_DMA_OK on success or UART_DMA_TX_ERROR_OVERFLOW if no sufficient space is available
 */
uart_dma_status_t uart_dma_reserve_space_tx(uart_dma_t *hudx, size_t len, size_t * pRef)
{
	size_t cnt;

	cnt = uart_dma_cnt_tx(hudx);

	// Determine free space
	size_t free = UART_DMA_TX_BUFFER_LEN - cnt;

	// Clip length if needed
	if (len > free) return UART_DMA_TX_ERROR_OVERFLOW;

	// Save reference
	*pRef = hudx->wAbsTX;

	// Advance pointer
	hudx->wAbsTX = advance_pointer_tx(hudx->wAbsTX, len);

	// Save number of newly added bytes
	hudx->nNewBytes += len;

	return UART_DMA_OK;
}

/**
 * \brief           Write into RX buffer relative to given reference
 *
 * 					Set bytes in RX buffer relative to given reference (absolute write pointer) if enough space is available. No read or write pointers are modified.
 *
 * \param[in]       hudx: Pointer to handle
 * \param[in]       offset: Number of bytes to advance before start writing
 * \param[in]      	data: Pointer to data array to be copied from
 * \param[in]       len: Number of bytes to write
 * \param[out]      ref: Reference to start from
 * \return          UART_DMA_OK on success or UART_DMA_RX_ERROR_OVERFLOW if no sufficient space is available (len > buffer size)
 */
uart_dma_status_t uart_dma_write_2_by_ref_rx(uart_dma_t *hudx, size_t offset, void* data, size_t len, size_t ref)
{
	size_t wRelTmp;

	// Sanity check
	if (len > UART_DMA_RX_BUFFER_LEN) return UART_DMA_RX_ERROR_OVERFLOW;

	// Use local variables
	wRelTmp = get_relative_pointer_rx(ref, offset);

	// Write data
	uart_dma_write(data, len, wRelTmp, hudx->huart->pRxBuffPtr, UART_DMA_RX_BUFFER_LEN);

	return UART_DMA_OK;
}

/**
 * \brief           Write into TX buffer relative to given reference
 *
 * 					Set bytes in TX buffer relative to given reference (absolute write pointer) if enough space is available. No read or write pointers are modified.
 *
 * \param[in]       hudx: Pointer to handle
 * \param[in]       offset: Number of bytes to advance before start writing
 * \param[in]      	data: Pointer to data array to be copied from
 * \param[in]       len: Number of bytes to write
 * \param[out]      ref: Reference to start from
 * \return          UART_DMA_OK on success or UART_DMA_TX_ERROR_OVERFLOW if no sufficient space is available (len > buffer size)
 */
uart_dma_status_t uart_dma_write_2_by_ref_tx(uart_dma_t *hudx, size_t offset, void* data, size_t len, size_t ref)
{
	size_t wRelTmp;

	// Sanity check
	if (len > UART_DMA_TX_BUFFER_LEN) return UART_DMA_TX_ERROR_OVERFLOW;

	// Use local variables
	wRelTmp = get_relative_pointer_tx(ref, offset);

	// Write data
	uart_dma_write(data, len, wRelTmp, hudx->pBuffTX, UART_DMA_TX_BUFFER_LEN);

	return UART_DMA_OK;
}

/**
 * \brief           Write byte into RX buffer relative to given reference
 *
 * 					Set byte in RX buffer relative to given reference (absolute write pointer). No read or write pointers are modified.
 *
 * \param[in]       hudx: Pointer to handle
 * \param[in]       offset: Number of bytes to advance before start writing
 * \param[in]      	data: Byte to set into buffer
 * \param[out]      ref: Reference to start from
 */
void uart_dma_put_by_ref_rx(uart_dma_t *hudx, size_t offset, uint8_t data, size_t ref)
{
	// Use local variables
	size_t wRelTmp = get_relative_pointer_rx(ref, offset);

	// Set byte
	hudx->huart->pRxBuffPtr[wRelTmp] = data;
}

/**
 * \brief           Write byte into TX buffer relative to given reference
 *
 * 					Set byte in TX buffer relative to given reference (absolute write pointer). No read or write pointers are modified.
 *
 * \param[in]       hudx: Pointer to handle
 * \param[in]       offset: Number of bytes to advance before start writing
 * \param[in]      	data: Byte to set into buffer
 * \param[out]      ref: Reference to start from
 */
void uart_dma_put_by_ref_tx(uart_dma_t *hudx, size_t offset, uint8_t data, size_t ref)
{
	// Use local variables
	size_t wRelTmp = get_relative_pointer_tx(ref, offset);

	// Set byte
	hudx->pBuffTX[wRelTmp] = data;
}

/**
 * \brief           Flush bytes in RX buffer by advancing read pointer
 * \note            Useful at the end of block processing
 * \param[in]       hudx: Pointer to handle
 * \param[in]       len: Number of bytes to flush
 * \return          Number of bytes flushed
 */
size_t uart_dma_flush_rx(uart_dma_t *hudx, size_t len)
{
	size_t cnt, r;

	// Check how much we can skip
	cnt = uart_dma_cnt_rx(hudx);
	if (len > cnt) len = cnt;

	// Work on local variables
	r = hudx->rAbsRX;

	// Advance pointer
	hudx->rAbsRX = advance_pointer_rx(r, len);

	return len;
}

/**
 * \brief           Flush bytes in TX buffer by advancing read pointer
 * \note            Useful at the end of block processing
 * \param[in]       hudx: Pointer to handle
 * \param[in]       len: Number of bytes to flush
 * \return          Number of bytes flushed
 */
size_t uart_dma_flush_tx(uart_dma_t *hudx, size_t len)
{
	size_t cnt, r;

	// Check how much we can skip
	cnt = uart_dma_cnt_tx(hudx);
	if (len > cnt) len = cnt;

	// Work on local variables
	r = hudx->rAbsTX;

	// Advance pointer
	hudx->rAbsTX = advance_pointer_tx(r, len);

	// Correct nNewBytes
	hudx->nNewBytes -= len;

	return len;
}

/**
 * \brief           Delete bytes in RX buffer by moving write pointer backwards

 * \param[in]       hudx: Pointer to handle
 * \param[in]       len: Number of bytes to delete
 * \return          Number of bytes deleted
 */
size_t uart_dma_delete_rx(uart_dma_t *hudx, size_t len)
{
	size_t cnt, w;

	// Check how much we can delete
	cnt = uart_dma_cnt_rx(hudx);
	if (len > cnt) len = cnt;

	// Work on local variables
	w = hudx->wAbsRX;

	// Backward pointer
	hudx->wAbsRX = backward_pointer_rx(w, len);

	return len;
}

/**
 * \brief           Delete bytes in TX buffer by moving write pointer backwards

 * \param[in]       hudx: Pointer to handle
 * \param[in]       len: Number of bytes to delete
 * \return          Number of bytes deleted
 */
size_t uart_dma_delete_tx(uart_dma_t *hudx, size_t len)
{
	size_t cnt, w;

	// Check how much we can delete
	cnt = uart_dma_cnt_tx(hudx);
	if (len > cnt) len = cnt;

	// Work on local variables
	w = hudx->wAbsTX;

	// Backward pointer
	hudx->wAbsTX = backward_pointer_tx(w, len);

	// Correct nNewBytes
	hudx->nNewBytes -= len;

	return len;
}

/**
 * \brief           Sends current content of TX buffer
 *
 * Send the current content of TX buffer and flushes current content after sending.
 * Bytes stored in TX buffer during ongoing send process are not sent and also not flushed.
 *
 * Some exemplary usages:
 *
 * \code{.c}

  // Write some bytes into buffer and send them
  uart_dma_write_2_tx(&hudx, msg, sizeof(msg));
  uart_dma_send(&hudx);

  // Wait until transmission is done
  while(hudx.huart->gState != HAL_UART_STATE_READY);

  // Write some new bytes into buffer and send them
  uart_dma_write_2_tx(&hudx, msg, sizeof(msg));
  uart_dma_send(&hudx);

  // While sending, put new bytes into buffer (which will not be sent automatically) and in case not sufficient space is available we wait here
  while(uart_dma_write_2_tx(&hudx, msg, sizeof(msg)) != UART_DMA_OK);

  // Send new bytes in buffer
  uart_dma_send(&hudx);

  // Put bytes into buffer as soon as space is available
  size_t nBytesToPut = sizeof(msg);
  while(nBytesToPut > 0)
  {
  	nBytesToPut -= uart_dma_write_1_tx(&hudx, &msg[sizeof(msg)-nBytesToPut], nBytesToPut);
  }

  // Send new bytes in buffer
  uart_dma_send(&hudx);

  // Put bytes byte-wise into buffer
  size_t cnt;
  for (cnt=0; cnt<sizeof(msg); cnt++)
  {
	while(uart_dma_put_tx(&hudx, msg[cnt]) != UART_DMA_OK);
  }

  // Send new bytes in buffer
  uart_dma_send(&hudx);
 *\endcode
 *
 * \param[in]       hudx: Pointer to handle
 * \return          Number of bytes which will be sent
 */
size_t uart_dma_send(uart_dma_t *hudx)
{
	// Update # of bytes to send
	hudx->nBytesToSend += hudx->nNewBytes;

	// If no transmission is ongoing we need to initiate one - This function is for sure entered only if ISR TxCplt already stopped DMA transmissions! So it is save this way!
	if (hudx->huart->gState == HAL_UART_STATE_READY)
	{
		// Determine linear length part
		uint16_t nBytesSending = (uint16_t) uart_dma_get_linear_block_length_tx(hudx, 0);

		// Update
		hudx->nBytesSent += nBytesSending;
		hudx->nBytesSending = nBytesSending;

		// Get current address
		uint8_t * pBuff = uart_dma_get_linear_block_address_tx(hudx, 0);

		// Start transmission - Set BUS pin to high for sending
		HAL_UART_Transmit_DMA(hudx->huart, pBuff, nBytesSending);
	}

	size_t retVal = hudx->nNewBytes;

	// Reset # of new bytes
	hudx->nNewBytes = 0;

	return retVal;
}

/**
 * \brief           Advance RX pointers

 * \param[in]       p: Pointer
 * \param[in]       offset: Advancement length
 * \return          Advanced pointer
 */
static size_t advance_pointer_rx(size_t p, size_t offset)
{
#ifdef UART_DMA_RX_BUFFER_LEN_POW_2
	p += offset;
#else // In case size of buffer is not a power of two we need to limit the indices to a value which is a multiple of UART_DMA_TX_BUFFER_LEN

	// We limit the index space of wAbs such that a correct wrap around happens
	// Check for a wrap around or if we are in unused index space - This has to be checked first!! We exploiting wrap around to correct index
	if ((p > p + offset) || (p + offset > UART_DMA_RX_BUFFER_MAX_PT_IDX))
	{
		p = (p + offset) + UART_DMA_RX_BUFFER_NON_USED_IDX_SPACE;
	}
	else
	{
		p += offset;
	}
#endif

	return p;
}

/**
 * \brief           Advance TX pointers

 * \param[in]       p: Pointer
 * \param[in]       offset: Advancement length
 * \return          Advanced pointer
 */
static size_t advance_pointer_tx(size_t p, size_t offset)
{
#ifdef UART_DMA_TX_BUFFER_LEN_POW_2
	p += offset;
#else // In case size of buffer is not a power of two we need to limit the indices to a value which is a multiple of UART_DMA_TX_BUFFER_LEN

	// We limit the index space of wAbs such that a correct wrap around happens
	// Check for a wrap around or if we are in unused index space - This has to be checked first!! We exploiting wrap around to correct index
	if ((p > p + offset) || (p + offset > UART_DMA_TX_BUFFER_MAX_PT_IDX))
	{
		p = (p + offset) + UART_DMA_TX_BUFFER_NON_USED_IDX_SPACE;
	}
	else
	{
		p += offset;
	}
#endif

	return p;
}

/**
 * \brief           Backward RX pointers

 * \param[in]       p: Pointer
 * \param[in]       offset: Backward length
 * \return          Backwarded pointer
 */
static size_t backward_pointer_rx(size_t p, size_t offset)
{
#ifdef UART_DMA_RX_BUFFER_LEN_POW_2
	p -= offset;
#else // In case size of buffer is not a power of two we need to limit the indices to a value which is a multiple of UART_DMA_TX_BUFFER_LEN

	// We limit the index space of wAbs such that a correct wrap around happens
	// Check for a wrap around or if we are in unused index space - This has to be checked first!! We exploiting wrap around to correct index
	if ((p < p - offset) || (p - offset > UART_DMA_RX_BUFFER_MAX_PT_IDX))
	{
		p = (p - offset) - UART_DMA_RX_BUFFER_NON_USED_IDX_SPACE;
	}
	else
	{
		p -= offset;
	}
#endif

	return p;
}

/**
 * \brief           Backward TX pointers

 * \param[in]       p: Pointer
 * \param[in]       offset: Backward length
 * \return          Backwarded pointer
 */
static size_t backward_pointer_tx(size_t p, size_t offset)
{
#ifdef UART_DMA_TX_BUFFER_LEN_POW_2
	p -= offset;
#else // In case size of buffer is not a power of two we need to limit the indices to a value which is a multiple of UART_DMA_TX_BUFFER_LEN

	// We limit the index space of wAbs such that a correct wrap around happens
	// Check for a wrap around or if we are in unused index space - This has to be checked first!! We exploiting wrap around to correct index
	if ((p < p - offset) || (p - offset > UART_DMA_TX_BUFFER_MAX_PT_IDX))
	{
		p = (p - offset) - UART_DMA_TX_BUFFER_NON_USED_IDX_SPACE;
	}
	else
	{
		p -= offset;
	}
#endif

	return p;
}

static inline size_t mod_substitute(size_t idx, size_t depth)
{
  //  while ( idx >= depth) idx -= depth;

  // We need at most two iterations - so we also could do this to avoid branching
  idx -= depth*(idx >= depth);
  idx -= depth*(idx >= depth);

  return idx;
}

/**
 * \brief           Convert absolute to relative RX pointer

 * \param[in]       p: Pointer
 * \param[in]       offset: Offset to current pointer position
 * \return          Relative pointer
 */
static size_t get_relative_pointer_rx(size_t p, size_t offset)
{
#ifdef UART_DMA_RX_BUFFER_LEN_POW_2
	return advance_pointer_rx(p, offset) & UART_DMA_RX_BUFFER_LEN_MASK;
#else
	return mod_substitute(advance_pointer_rx(p, offset), UART_DMA_RX_BUFFER_LEN);
#endif
}

/**
 * \brief           Convert absolute to relative TX pointer

 * \param[in]       p: Pointer
 * \param[in]       offset: Offset to current pointer position
 * \return          Relative pointer
 */
static size_t get_relative_pointer_tx(size_t p, size_t offset)
{
#ifdef UART_DMA_TX_BUFFER_LEN_POW_2
	return advance_pointer_tx(p, offset) & UART_DMA_TX_BUFFER_LEN_MASK;
#else
	return mod_substitute(advance_pointer_tx(p, offset), UART_DMA_TX_BUFFER_LEN);
#endif
}

/** @} */
