/** @addtogroup  group_rero RERO
 *  @{
 */

/**
 * \brief 			Rero transmission protocol implementation
 * \details			C implementation of the rero transmission protocol. A Python implementation is also [available](https://pypi.org/project/rero/).
 * \author 			Reinhard Panhuber
 * \version 		1.0.0
 * \date 			04.10.2019
 * \file 			rero.c
 * \copyright  		MIT
 */

#include "rero.h"
#include "math.h"

static void rero_reset_rx_buffer_and_sm(rero_t *hrero);
static uint8_t rero_decode_cobs_tiny(rero_t *hrero, size_t off, size_t len, uint8_t cobsCode);
static size_t rero_big_to_little_endianess(rero_t *hrero, size_t off, size_t len);
static uint8_t strip_non_eof_delimiter(rero_t *hrero);
static uint8_t strip_eof_delimiter(rero_t *hrero);
static size_t rero_decode_frame_to(rero_t *hrero, uint8_t * data);
static uint8_t rero_decode_frame_in_buffer_lin_part(rero_t *hrero, size_t linLen, uint8_t *pBuff, uint8_t *escOccureFlag, uint8_t cobsCode, size_t *idxRead, size_t *idxWrite);
static size_t rero_decode_frame_in_buffer_lin_finish(rero_t *hrero, size_t len, uint8_t *pBuff, uint8_t escOccureFlag, uint8_t cobsCode, size_t idxRead, size_t idxWrite);
static uint8_t rero_decode_frame_in_buffer_wrap(rero_t *hrero, size_t len, uint8_t *pBuff, uint8_t *escOccureFlag, uint8_t cobsCode, size_t *idxRead, size_t *idxWrite);
static size_t rero_decode_frame_in_buffer_wrap_finish(rero_t *hrero, size_t len, uint8_t *pBuff, uint8_t escOccureFlag, uint8_t cobsCode, size_t idxRead, size_t idxWrite);
static uint8_t rero_decode_frame_to_lin_part(rero_t *hrero, uint8_t * restrict data, size_t linLen, uint8_t *pBuff, uint8_t cobsCode, size_t *idxRead, size_t *idxWrite);
static size_t rero_decode_frame_to_lin_finish(rero_t *hrero, uint8_t * restrict data, size_t len, uint8_t *pBuff, uint8_t cobsCode, size_t idxRead, size_t idxWrite);
static uint8_t rero_decode_frame_to_wrap(rero_t *hrero, uint8_t * restrict data, size_t len, uint8_t *pBuff, uint8_t cobsCode, size_t *idxRead, size_t *idxWrite);
static size_t rero_decode_frame_to_wrap_finish(rero_t *hrero, uint8_t * restrict data, size_t len, uint8_t *pBuff, uint8_t cobsCode, size_t idxRead, size_t idxWrite);

#if defined(STM32F411xE)

typedef enum
{
	CRC_COMPUTATION, 				//!< Indicates normal operation i.e. valid state with no errors
	CRC_VERIFICATION 				//!< TX buffer has no sufficient space to hold encoded RERO frame
} rero_crc_t;

static uint32_t CRC_handle_8_plus_wrap(CRC_HandleTypeDef *hcrc, uint8_t *pBuffLin, uint8_t *pBuffWrap, size_t lenLin, size_t lenTot, rero_crc_t type);
static uint32_t CRC_soft(uint32_t crc, uint8_t data);

#endif

/**
 * \brief           Initialize rero construct
 * \param[in]       hrero: Pointer to rero structure
 * \param[in]       hcrc: Pointer to CRC handle
 * \param[in]       hudx: Pointer to uart_dma handle
 * \param[in]       htim6: Pointer to timer handle
 * \param[in]       address: Address of this node
 * \return          `RERO_OK` on success, `RERO_ERROR_INIT` otherwise
 */
rero_status_t rero_init(rero_t *hrero, CRC_HandleTypeDef *hcrc, uart_dma_t *hudx, TIM_HandleTypeDef *htim6, size_t address)
{
	assert_param(hrero != NULL && hcrc != NULL);

	if (hrero == NULL || hcrc == NULL) {
		return RERO_ERROR_INIT;
	}

	memset(hrero, 0, sizeof(*hrero));
	hrero->hcrc = hcrc;
	hrero->hudx = hudx;
	hrero->htim = htim6;
	hrero->address = address;
	rero_reset_rx_buffer_and_sm(hrero);

	return RERO_OK;
}

/**
 * \brief 			Routine where the receive state machine is implemented
 *
 * 					This function implements the receive state machine. It
 * 					has to be polled on a regular basis to update states
 * 					(within the timeout limit). It returns various state messages:
 *
 *					RERO_OK: 			All okay. Either nothing new happened or a part of a frame was received or a complete frame was received. To determine if a frame is available call rero_is_frame_available().
 *					RERO_ERROR_CRC: 	Received a complete frame but CRC check failed - frame is discarded.
 * 					RERO_OTHER_REC: 	Received a frame intended for a different node (different node address).
 * 					RERO_ERROR_SYNC: 	Sync. error happened - this function tries to resynchronize on its own when called again.
 * 					RERO_ERROR_TIMEOUT:	Timeout happened during reception of frame. Partly received frame gets discarded.
 *
 * \param[in]       hrero: Pointer to RX state machine structure
 */

rero_status_t rero_check_rx(rero_t *hrero)
{
	// RX buffer line-up: | frame length + COBS length code of CRC | COBSR(address + payload) | CRC | EOF |

	// First check if we need to synchronize
	if (hrero->sms == RERO_Out_Of_Sync)
	{
		// We need to synchronize by scanning through the buffer to find an EOF delimiter and skip all which is before
		if (strip_non_eof_delimiter(hrero))
		{
			// Found an EOF delimiter - strip the rest of possible following EOF delimiter
			if (strip_eof_delimiter(hrero))
			{
				hrero->sms = RERO_Rec_Header; 			// We are synchronized
			}
		}
	}

	// If we are synchronized
	if (hrero->sms != RERO_Out_Of_Sync)
	{
		// If timeout happened - reset everything and return error code
		if (hrero->timeoutFlag == 0)
		{
			// If we do not wait for a frame to be processed
			if (hrero->sms != RERO_Frame_Available)
			{
				// Get number of bytes in buffer
				size_t len = uart_dma_cnt_rx(hrero->hudx);

				// If there is something new, stop timeout and proceed checking
				if (len > hrero->nBytesRcvd)
				{
					HAL_TIM_Base_Stop_IT(hrero->htim);

					// Reload counter register
					hrero->htim->Instance->CNT = 0;

					// Update number of bytes received
					hrero->nBytesRcvd = len;

					// Check if header was received completely
					if (hrero->sms == RERO_Rec_Header && len >= RERO_AddFrameFieldLen + RERO_FrameLenCheck + RERO_AddrFieldLen + 1)
					{
						// Check for validity of length field (which is not covered by CRC field)

						// Check if there is any EOF delimiter byte contained within length field - this is necessary for improved robustness and is not expensive
						size_t cnt;
						for (cnt = 0; cnt < RERO_AddFrameFieldLen + RERO_FrameLenCheck + 1; cnt++) // RERO_AddFrameFieldLen ERROR!
						{
							if (*((uint8_t *) uart_dma_get_linear_block_address_rx(hrero->hudx, cnt)) == RERO_EOF)
							{
								// We have found an invalid EOF delimiter - we are desynchronized - flush all until (including) the first EOF delimiter and set flag - the rest will be taken care of by the resynchronization part above
								uart_dma_flush_rx(hrero->hudx, cnt+1);
								hrero->sms = RERO_Out_Of_Sync;
								hrero->nBytesRcvd = 0;
								return RERO_ERROR_SYNC;
							}
						}

						// Decode the header

						// Rebuild first cobs code and first frame length byte
						uint8_t cobsCode;

						uart_dma_peek_byte_rx(hrero->hudx, 0, &cobsCode); 									// Read first byte
						uart_dma_modify_byte_rx(hrero->hudx, 0, cobsCode & RERO_FirstManByteMask);	// Rebuild first length byte in buffer
						cobsCode = (cobsCode & RERO_FirstCobsCodeMask) >> (8 - RERO_FirstCobsCodeNBits); 	// Rebuild first cobs code

						// Decode rest of header in buffer
						cobsCode = rero_decode_cobs_tiny(hrero, 0, RERO_AddFrameFieldLen + RERO_AddrFieldLen + 1, cobsCode);
#if RERO_CRCLen > 0
						// Save cobs code for decoding of CRC
						hrero->decoCobsCode = cobsCode;
#endif
						// Conduct further check on length field if required
#if RERO_FrameLenCheck == 1
						uint8_t checkSum = 0;
						for (cnt = 0; cnt < RERO_AddFrameFieldLen+1; cnt++)
						{
							checkSum += *((uint8_t *) uart_dma_get_linear_block_address_rx(hrero->hudx, cnt));
						}

						// Combine with received checksum and check
						if ((checkSum + *((uint8_t *) uart_dma_get_linear_block_address_rx(hrero->hudx, RERO_AddFrameFieldLen+1))) != 0xFF)
						{
							// Check not successful - flush buffer and set to unsynchronized status
							uart_dma_flush_rx(hrero->hudx, RERO_AddFrameFieldLen+1+RERO_FrameLenCheck);
							hrero->sms = RERO_Out_Of_Sync;
							hrero->nBytesRcvd = 0;
							return RERO_ERROR_SYNC;
						}
#endif
						// Rebuild length field
						hrero->frameLen = rero_big_to_little_endianess(hrero, 0, RERO_AddFrameFieldLen+1);

						// Advance state
						hrero->sms = RERO_Rec_Rest;
					}

					if (hrero->sms == RERO_Rec_Rest && len >= hrero->frameLen + RERO_AddFrameFieldLen + RERO_FrameLenCheck + 2 + RERO_AddrFieldLen + RERO_CRCLen) // Total frame length, +1 due to EOF delimiter at the end
					{
						hrero->sms = RERO_Check_Frame;
					}

					if (hrero->sms == RERO_Check_Frame)
					{
						// Check if EOF delimiter is at given position
						if (*((uint8_t *) uart_dma_get_linear_block_address_rx(hrero->hudx, hrero->frameLen + RERO_AddFrameFieldLen + RERO_FrameLenCheck + RERO_AddrFieldLen + RERO_CRCLen + 1)) != RERO_EOF)
						{
							// Out of synchronization error - reset and return error
							rero_reset_rx_buffer_and_sm(hrero);
							hrero->sms = RERO_Out_Of_Sync;
							return RERO_ERROR_SYNC;
						}

						// Check address if required
#if RERO_AddrFieldLen > 0
						size_t addr = rero_big_to_little_endianess(hrero, 1+RERO_AddFrameFieldLen+RERO_FrameLenCheck, RERO_AddrFieldLen);
						if ((addr != 0) && (addr != hrero->address))
						{
							// This frame is not for us so reset and return
							rero_reset_rx_buffer_and_sm(hrero);
							return RERO_OTHER_REC;
						}
#endif

						// Decode and check CRC if required
#if RERO_CRCLen > 0

						// Decode the CRC code in the buffer
						rero_decode_cobs_tiny(hrero, 1 + RERO_AddFrameFieldLen + RERO_FrameLenCheck + RERO_AddrFieldLen + hrero->frameLen, RERO_CRCLen, hrero->decoCobsCode);

						// CRC rebuilt - check CRC - the address field is also CRC covered
						// Get buffer and length
						uint8_t *pBuff = uart_dma_get_linear_block_address_rx(hrero->hudx, RERO_AddFrameFieldLen + RERO_FrameLenCheck + 1);
						len = uart_dma_get_linear_block_length_rx(hrero->hudx, RERO_AddFrameFieldLen + RERO_FrameLenCheck + 1);

						// If current # of bytes available is bigger than those to be parsed we need to clip
						if (len > hrero->frameLen + RERO_CRCLen + RERO_AddrFieldLen)
						{
							len = hrero->frameLen + RERO_CRCLen + RERO_AddrFieldLen;
						}

#if defined(STM32F303x8)

						// Conduct partial CRC for first linear block
						uint32_t crc = HAL_CRC_Accumulate(hrero->hcrc, (uint32_t*) pBuff, len);

						// Check if there is a wrap around necessary
						if(hrero->frameLen + RERO_CRCLen + RERO_AddrFieldLen > len)
						{
							// We know that we need the beginning of the RX buffer. Instead of calculating it we simply get it. This is a slightly nasty style here but simply faster.
							crc = HAL_CRC_Accumulate(hrero->hcrc, (uint32_t*) hrero->hudx->huart->pRxBuffPtr, hrero->frameLen + RERO_CRCLen + RERO_AddrFieldLen - len);
						}

						if(crc != 0)
						{
							// CRC Error - reset and return
							rero_reset_rx_buffer_and_sm(hrero);
							return RERO_ERROR_CRC;
						}

						// Reset the CRC module since this may be used by rero_send()
						WRITE_REG(hrero->hcrc->Instance->INIT, DEFAULT_CRC_INITVALUE);
#endif // STM32F303x8

#if defined(STM32F411xE)

						// Conduct CRC check
						uint32_t crc = CRC_handle_8_plus_wrap(hrero->hcrc, pBuff, hrero->hudx->huart->pRxBuffPtr, len, hrero->frameLen + RERO_CRCLen + RERO_AddrFieldLen, CRC_VERIFICATION);

						if(crc != 0)
						{
							// CRC Error - reset and return
							rero_reset_rx_buffer_and_sm(hrero);
							return RERO_ERROR_CRC;
						}

						// Reset the CRC module since this may be used by rero_send()
						__HAL_CRC_DR_RESET(hrero->hcrc);
#endif // STM32F411xE

#endif // RERO_CRCLen > 0

						// Ok we have a frame - skip the header
						uart_dma_flush_rx(hrero->hudx, RERO_AddFrameFieldLen + 1 + RERO_FrameLenCheck + RERO_AddrFieldLen);

						// Set final state
						hrero->sms = RERO_Frame_Available;
						return RERO_OK;
					}

					// If we end up her stuff is left to be received - start the timeout
					HAL_TIM_Base_Start_IT(hrero->htim);
				}
			}
		}
		else
		{
			// Timeout happened - reset everything and return error code
			rero_reset_rx_buffer_and_sm(hrero);
			return RERO_ERROR_TIMEOUT;
		}

		return RERO_OK;
	}
	else
	{
		return RERO_ERROR_SYNC;
	}
}

/**
 * \brief 			Checks if a frame was received
 * \param[in]       hrero: Pointer to RX state machine structure
 * \return          1 if available, 0 else
 */
size_t rero_is_frame_available(rero_t *hrero)
{
	if (hrero->sms == RERO_Frame_Available)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

/**
 * \brief 			Notify frame processed
 *
 * 					Notify the rero framework that a received frame was processed. This informs rero to dump this frame and to provide the next received frame.
 *
 * \param[in]       hrero: Pointer to RX state machine structure
 */
void rero_notify_frame_processed(rero_t *hrero)
{
	// Reset state machine and allow to check again
	rero_reset_rx_buffer_and_sm(hrero);
}

/**
 * \brief Reset RX Buffer and rero State Machine
 *
 * Resets the RX Buffer and rero state machine, intended to execute when timeout occurs
 *
 * \param[in]       hrero: Pointer to RX state machine structure
 */
void rero_reset_rx_buffer_and_sm(rero_t *hrero)
{
	// Stop timeout and reset counter value
	HAL_TIM_Base_Stop_IT(hrero->htim);
	hrero->htim->Instance->CNT = 0;

#if RERO_CRCLen > 0
	// Reset CRC Init value
	//	WRITE_REG(hrero->hcrc->Instance->INIT, DEFAULT_CRC_INITVALUE);
	// This should work for STM32F4
	// Reset CRC Calculation Unit (hcrc->Instance->INIT is written in hcrc->Instance->DR) -> from HAL... this is bullshit!
	__HAL_CRC_DR_RESET(hrero->hcrc);
#endif

	// Flush RX buffer
	size_t toFlush;

	// In case we reset from a proper received frame we have to flush less
	if (hrero->sms == RERO_Frame_Available)
	{
		toFlush = hrero->frameLen + RERO_CRCLen + 1; 	// +1 for EOF delimiter
	}
	else
	{
		toFlush = hrero->frameLen + RERO_CRCLen + RERO_AddrFieldLen + RERO_AddFrameFieldLen + RERO_FrameLenCheck + 2; 	// +1 for EOF delimiter
	}

	// We may need to clip in case there is less to flush - this happens in case we have received only a part of a frame
	if (toFlush > hrero->nBytesRcvd)
	{
		toFlush = hrero->nBytesRcvd;
	}
	uart_dma_flush_rx(hrero->hudx,toFlush);

	// Reset states
	hrero->sms = RERO_Rec_Header;
	hrero->frameLen = 0;
	hrero->frameLenDeco = 0;
	hrero->nBytesRcvd = 0; 				// This is save here since next time rero_check is called this value is updated again
	hrero->timeoutFlag = 0;
	hrero->frameDecodedFlag = 0;
	hrero->iterIdx = 0;
	hrero->iterLim = 0;
	hrero->iterCobsCode = 0xff;
	hrero->iterFlag = 0;
}

/**
 * \brief 			Timeout notification
 *
 * 					This function has to be called from the timer ISR callback routine in order to inform rero that a timeout happened.
 *
 * \param[in]       hrero: Pointer to RX state machine structure
 */
void rero_timeout_notification(rero_t *hrero)
{
	hrero->timeoutFlag = 1;
}

/**
 * \brief Copy received frame to destination
 *
 * A frame is copied to destination given. The frame gets either decoded on the
 * fly or if already decoded in buffer simply copied to given destination. The size of the bytes written can be determined (if required) by the function rero_get_decoded_frame_length(rero_t *hrero).
 *
 * \param[in]       hrero: Pointer to RX state machine structure
 * \param[out]      data: Pointer to data to copy memory to
 * \return          Number of bytes written
 */
size_t rero_copy_frame(rero_t *hrero, void* data)
{
	if (hrero->sms == RERO_Frame_Available)
	{
		if (hrero->frameDecodedFlag)
		{
			// Frame already decoded in buffer so copy straight away
			size_t len = uart_dma_get_linear_block_length_rx(hrero->hudx, 0);

			// We may need to clip
			if (len > hrero->frameLenDeco)
			{
				len = hrero->frameLenDeco;
			}

			uart_dma_peek_rx(hrero->hudx, 0, data, len);

			// Check if there is a wrap around necessary
			if(hrero->frameLenDeco > len)
			{
				uart_dma_peek_rx(hrero->hudx, len, (void *)&(((uint8_t *)data)[len]), hrero->frameLenDeco - len);
			}

			return hrero->frameLenDeco;
		}
		else
		{
			return rero_decode_frame_to(hrero, data);
		}
	}
	else
	{
		return 0;
	}
}

/**
 * \brief Get linear block length of received frame
 *
 * Returns the lengths corresponding to given pointers which may be read in a linear manner.
 * This is of major interest for DMA transmissions. If returned lengths are zero the
 * corresponding pointers are invalid. If not already done, the received frame is decoded
 * in buffer first before returning pointers.
 *
 * \param[in]       hrero: Pointer to RX state machine structure
 * \param[in]       *len1: Length of first linear part. If zero corresponding pointer p1 is invalid
 * \param[in]       *len2: Length of second linear part. If zero corresponding pointer p2 is invalid
 * \param[in]       **p1: Pointer to start read of first part
 * \param[in]       **p2: Pointer to start read of second part
 */
void rero_get_linear_read_info(rero_t *hrero, size_t *len1, size_t * len2, void ** p1, void ** p2)
{
	if (hrero->sms == RERO_Frame_Available)
	{
		// If buffer was not already decoded in buffer do so now
		if (!hrero->frameDecodedFlag)
		{
			rero_decode_frame_in_buffer(hrero);
		}

		size_t len = uart_dma_get_linear_block_length_rx(hrero->hudx, 0);

		// We may need to clip
		if (len > hrero->frameLenDeco)
		{
			len = hrero->frameLenDeco;
		}

		*len1 = len;
		*p1 = uart_dma_get_linear_block_address_rx(hrero->hudx, 0);

		// Check if there is a wrap around necessary
		if(hrero->frameLenDeco > len)
		{
			*len2 = hrero->frameLenDeco - len;
			*p2 = hrero->hudx->huart->pRxBuffPtr;
		}
		else
		{
			*len2 = 0;
		}
	}
	else
	{
		*len1 = *len2 = 0;
	}
}

/**
 * \brief Get length of decoded frame
 *
 * Determines the length of a frame once decoded. The received frame need not to be already decoded!
 *
 * \param[in]       hrero: Pointer to RX state machine structure
 * \return          Length of decoded frame in bytes. In case no frame was received zero is returned
 */
size_t rero_get_decoded_frame_length(rero_t *hrero)
{
	if (hrero->sms == RERO_Frame_Available)
	{

		// Here it may be possible that the decoded frame length was already determined by a former call to a decoding function. We assume that if  frameLenDeco > 0 this decoding step was already done. However it may also be done in case frameLenDeco = 0 which is also valid.

		if (hrero->frameLenDeco == 0)
		{
			// We need to scan for the # of 255 bytes and at the end if COBSR further reduced frame length for one byte
			uint8_t *pBuff = uart_dma_get_linear_block_address_rx(hrero->hudx, 0);

			// Get # bytes in buffer accessible linearly
			size_t len = uart_dma_get_linear_block_length_rx(hrero->hudx, 0);

			// Clip to frame length
			if (len > hrero->frameLen)
			{
				len = hrero->frameLen;
			}

			size_t decoFrameLen = hrero->frameLen - 1;
			size_t idx = pBuff[0]; 	// First length code is always extra and does not count here

			while (idx < len)
			{
				// Check
				if (pBuff[idx] == 255)
				{
					decoFrameLen--;
				}

				idx += pBuff[idx];
			}

			// If not all done proceed
			if (len < hrero->frameLen)
			{
				// Treat wrap around in idx
				idx -= len;

				// Calculate remaining length
				len = hrero->frameLen - len;

				// Get pointer at beginning of buffer
				pBuff = hrero->hudx->huart->pRxBuffPtr;

				while (idx < len)
				{
					// Check
					if (pBuff[idx] == 255)
					{
						decoFrameLen--;
					}

					idx += pBuff[idx];
				}
			}

			// Check if COBSR is applicable
			// Have a look if COBSR is applicable
			if (idx > len)
			{
				decoFrameLen++;
			}

			hrero->frameLenDeco = decoFrameLen;

			return decoFrameLen;
		}
		else
		{
			return hrero->frameLenDeco;
		}

	}
	else
	{
		return 0;
	}
}

/**
 * \brief Iterate over frame
 *
 * Iterates byte wise and decodes received frame on the fly. Useful if frame is consumed byte by byte.
 * If a frame was iterated over completely zero is returned. On next call of rero_iterate() the
 * iteration starts again from the beginning.
 *
 * \param[in]       hrero: Pointer to RX state machine structure
 * \param[out]      byteOut: Pointer to copy byte to
 * \return          1 if byte is copied, else no byte copied and nothing to copy
 */
uint8_t rero_iterate(rero_t *hrero, uint8_t * byteOut)
{
	// Check if we may already proceed
	if (hrero->iterIdx < hrero->iterLim)
	{
		// Return value and advance iterator index
		uart_dma_peek_byte_rx(hrero->hudx, hrero->iterIdx++, byteOut);

		return 1;
	}
	else
	{
		// Check if we can proceed
		if (hrero->sms == RERO_Frame_Available && hrero->frameLen > 0) // The second check is necessary in case empty
		{
			if (hrero->frameDecodedFlag == 0)
			{
				if (hrero->iterFlag == 0)
				{
					// Remember old COBS code
					uint8_t cobsOld = hrero->iterCobsCode;

					// Save new COBS code
					uart_dma_peek_byte_rx(hrero->hudx, hrero->iterIdx++, &hrero->iterCobsCode);

					size_t remBytes = hrero->frameLen - hrero->iterIdx;

					// Check if we have to write a zero
					if (cobsOld != 0xff)
					{
						// Return a zero byte
						*byteOut = 0;
					}
					else
					{
						// Return next byte
						uart_dma_peek_byte_rx(hrero->hudx, hrero->iterIdx++, byteOut);
					}

					if (hrero->iterCobsCode - 1 < remBytes)
					{
						// Update limit
						hrero->iterLim += hrero->iterCobsCode;
					}
					else
					{
						// This is entered in the last segment and that only once - indicate that by setting flag
						hrero->iterFlag++;

						// Check for COBSR - we have to do this now
						if (hrero->iterCobsCode - 1 > remBytes)
						{
							// Remember that COBSR must be applied at the end
							hrero->iterFlag++;
						}

						hrero->iterLim += remBytes+1;
					}

					return 1;
				}
				else
				{
					if (hrero->iterFlag > 1)
					{
						// COBSR
						*byteOut = hrero->iterCobsCode;
						hrero->iterFlag--;
						return 1;
					}

					// We are done - reset iteration stuff
					hrero->iterIdx = 0;
					hrero->iterLim = 0;
					hrero->iterCobsCode = 0xff;
					hrero->iterFlag = 0;

					return 0;
				}
			}
			else
			{
				// Check if this is the first visit
				if (hrero->iterIdx == 0)
				{
					// Update iteration length
					hrero->iterLim = hrero->frameLenDeco;

					// Read first byte
					uart_dma_peek_byte_rx(hrero->hudx, hrero->iterIdx++, byteOut);

					return 1;
				}
				else
				{
					// This marks the end - reset and return
					hrero->iterIdx = 0;
					hrero->iterLim = 0;
					return 0;
				}
			}

		}
		else
		{
			return 0;
		}
	}
}

/**
 * \brief Decode received frame in buffer
 *
 * Decodes the received frame in the buffer which is useful if you need to read out decoded frame
 * in one (or two) reads e.g. for a DMA transmission.
 *
 * \param[in]       hrero: Pointer to RX state machine structure
 * \return          Length of decoded frame in bytes. In case no frame was received zero is returned
 */
size_t rero_decode_frame_in_buffer(rero_t *hrero)
{
	if (hrero->sms == RERO_Frame_Available && hrero->frameDecodedFlag == 0)
	{
		size_t idxRead = 1; 	// We start at one since the first byte at index zero is the first COBS code
		size_t idxWrite = 1;

		// Access buffer
		uint8_t *pBuff = uart_dma_get_linear_block_address_rx(hrero->hudx, 0);

		// Get # bytes in buffer accessible linearly
		size_t len = uart_dma_get_linear_block_length_rx(hrero->hudx, 0);

		uint8_t wrapFlag;

		// Determine if a wrap happens or not and adjust length
		if (len >= hrero->frameLen)
		{
			wrapFlag = 0;
			len = hrero->frameLen;
		}
		else
		{
			wrapFlag = 1;
		}

		uint8_t escOccureFlag = 0; 		// We need to know if we have to copy in buffer which is the case if 0xff was received

		uint8_t cobsCode = *pBuff;

		// Handle first linear part
		cobsCode = rero_decode_frame_in_buffer_lin_part(hrero, len, pBuff, &escOccureFlag, cobsCode, &idxRead, &idxWrite);

		// Handle rest
		if (wrapFlag)
		{
			// Check if a common wrap or a wrap finish happens
			if ((cobsCode - 1) >= (hrero->frameLen - idxRead))
			{
				// We have a wrap finish
				idxWrite = rero_decode_frame_in_buffer_wrap_finish(hrero, len, pBuff, escOccureFlag, cobsCode, idxRead, idxWrite);
			}
			else
			{
				// Common wrap
				cobsCode = rero_decode_frame_in_buffer_wrap(hrero, len, pBuff, &escOccureFlag, cobsCode, &idxRead, &idxWrite);

				// Update length
				len = hrero->frameLen - len;

				// Update pointer to buffer
				pBuff = hrero->hudx->huart->pRxBuffPtr;

				// Consecutive linear parts
				cobsCode = rero_decode_frame_in_buffer_lin_part(hrero, len, pBuff, &escOccureFlag, cobsCode, &idxRead, &idxWrite);

				// Finish linear part
				idxWrite = rero_decode_frame_in_buffer_lin_finish(hrero, len, pBuff, escOccureFlag, cobsCode, idxRead, idxWrite);
			}

		}
		else
		{
			// No wrap occurs so conduct a linear finish
			idxWrite = rero_decode_frame_in_buffer_lin_finish(hrero, len, pBuff, escOccureFlag, cobsCode, idxRead, idxWrite);
		}

		// Skip first bytes since this is the very first COBS code and not needed any more
		uart_dma_flush_rx(hrero->hudx, 1);

		// Update frame length in buffer for proper reset of buffer
		hrero->frameLen--;

		// Mark frame as decoded
		hrero->frameDecodedFlag = 1;

		hrero->frameLenDeco = idxWrite-1;
		return idxWrite-1; 	// return length of bytes written
	}
	else
	{
		return 0;
	}
}

/**
 * \brief Copy a decoded frame to pointer given
 *
 * A decoded frame will be copied to pointer given.
 *
 * \param[in]       hrero: Pointer to RX state machine structure
 * \param[in]       data: Pointer where decoded frame is to copy
 * \return          Length of decoded frame in bytes. In case no frame was received zero is returned
 */
static size_t rero_decode_frame_to(rero_t *hrero, uint8_t * restrict data)
{
	if (hrero->sms == RERO_Frame_Available && hrero->frameDecodedFlag == 0)
	{
		size_t idxRead = 1; 	// We start at one since the first byte at index zero is the first COBS code
		size_t idxWrite = 0;

		// Access buffer
		uint8_t *pBuff = uart_dma_get_linear_block_address_rx(hrero->hudx, 0);

		// Get # bytes in buffer accessible linearly
		size_t len = uart_dma_get_linear_block_length_rx(hrero->hudx, 0);

		uint8_t wrapFlag;

		// Determine if a wrap happens or not and adjust length
		if (len >= hrero->frameLen)
		{
			wrapFlag = 0;
			len = hrero->frameLen;
		}
		else
		{
			wrapFlag = 1;
		}

		uint8_t cobsCode = *pBuff;

		// Handle first linear part
		cobsCode = rero_decode_frame_to_lin_part(hrero, data, len, pBuff, cobsCode, &idxRead, &idxWrite);

		// Handle rest
		if (wrapFlag)
		{
			// Check if a common wrap or a wrap finish happens
			if ((cobsCode - 1) >= (hrero->frameLen - idxRead))
			{
				// We have a wrap finish
				idxWrite = rero_decode_frame_to_wrap_finish(hrero, data, len, pBuff, cobsCode, idxRead, idxWrite);
			}
			else
			{
				// Common wrap
				cobsCode = rero_decode_frame_to_wrap(hrero, data, len, pBuff, cobsCode, &idxRead, &idxWrite);

				// Update length
				len = hrero->frameLen - len;

				// Update pointer to buffer
				pBuff = hrero->hudx->huart->pRxBuffPtr;

				// Consecutive linear parts
				cobsCode = rero_decode_frame_to_lin_part(hrero, data, len, pBuff, cobsCode, &idxRead, &idxWrite);

				// Finish linear part
				idxWrite = rero_decode_frame_to_lin_finish(hrero, data, len, pBuff, cobsCode, idxRead, idxWrite);
			}

		}
		else
		{
			// No wrap occurs so conduct a linear finish
			idxWrite = rero_decode_frame_to_lin_finish(hrero, data, len, pBuff, cobsCode, idxRead, idxWrite);
		}

		hrero->frameLenDeco = idxWrite;
		return idxWrite; 	// return length of bytes written
	}
	else
	{
		return 0;
	}
}

/**
 * \brief Decode received frame in buffer - linear part
 *
 * This is a support function for rero_decode_frame_in_buffer

 * \return          Last COBS code found
 */
static uint8_t rero_decode_frame_to_lin_part(rero_t *hrero, uint8_t * restrict data, size_t linLen, uint8_t *pBuff, uint8_t cobsCode, size_t *idxRead, size_t *idxWrite)
{
	size_t remLen = linLen - *idxRead;
	uint8_t cobsCodeOld;

	while ((cobsCode-1) < remLen)
	{
		memcpy((void *)(&data[*idxWrite]), &pBuff[*idxRead], cobsCode-1);

		// Update indices
		*idxRead += cobsCode-1;
		*idxWrite += cobsCode-1;

		// Save new COBS code before it gets overwritten
		cobsCodeOld = cobsCode;
		cobsCode = pBuff[(*idxRead)++];

		// Add a zero to the end
		if (cobsCodeOld != 0xFF)
		{
			data[(*idxWrite)++] = 0;
		}

		// Update remaining length
		remLen = linLen - *idxRead;
	}

	return cobsCode;
}

/**
 * \brief Decode received frame in buffer - linear finish
 *
 * This is a support function for rero_decode_frame_in_buffer

 * \return          Last write index
 */
static size_t rero_decode_frame_to_lin_finish(rero_t *hrero, uint8_t * restrict data, size_t len, uint8_t *pBuff, uint8_t cobsCode, size_t idxRead, size_t idxWrite)
{
	size_t remLen = len-idxRead;

	// Last COBS code reached - write remaining bytes
	memcpy((void *)(&data[idxWrite]), &pBuff[idxRead], remLen);

	// Update indices
	idxWrite += remLen;

	// Write final data byte if applicable for COBSR
	if (cobsCode - 1 > remLen)
	{
		data[idxWrite++] = cobsCode;
	}

	return idxWrite;
}

/**
 * \brief Decode received frame in buffer - wrap
 *
 * This is a support function for rero_decode_frame_in_buffer

 * \return          Last COBS code found
 */
static uint8_t rero_decode_frame_to_wrap(rero_t *hrero, uint8_t * restrict data, size_t len, uint8_t *pBuff, uint8_t cobsCode, size_t *idxRead, size_t *idxWrite)
{
	uint8_t cobsCodeOld;

	len -= *idxRead; 					// Determine remaining length

	// Copy first part
	memcpy((void *)(&data[*idxWrite]), &pBuff[*idxRead], len);

	// Update indices
	*idxRead = 0;
	*idxWrite += len;

	// Update pointer to buffer
	pBuff = hrero->hudx->huart->pRxBuffPtr;

	// Update length
	len = cobsCode - 1 - len;

	// Copy part two
	memcpy((void *)(&data[*idxWrite]), pBuff, len);

	// Update indices
	*idxRead += len;
	*idxWrite += len;

	// Update cobs code
	cobsCodeOld = cobsCode;
	cobsCode = pBuff[(*idxRead)++];

	// Add a zero to the end
	if (cobsCodeOld != 0xFF)
	{
		data[(*idxWrite)++] = 0;
	}

	return cobsCode;
}

/**
 * \brief Decode received frame in buffer - wrap finish
 *
 * This is a support function for rero_decode_frame_in_buffer

 * \return          Last write index
 */
static size_t rero_decode_frame_to_wrap_finish(rero_t *hrero, uint8_t * restrict data, size_t len, uint8_t *pBuff, uint8_t cobsCode, size_t idxRead, size_t idxWrite)
{
	size_t len2 = hrero->frameLen - len; 	// Determine remaining length of second part

	len -= idxRead; 						// Determine remaining length of first part

	// Copy first part
	memcpy((void *)(&data[idxWrite]), &pBuff[idxRead], len);

	// Update indices
	idxRead = 0;
	idxWrite += len;

	// Update pointer to buffer
	pBuff = hrero->hudx->huart->pRxBuffPtr;

	// Copy part two
	memcpy((void *)(&data[idxWrite]), pBuff, len2);

	// Update indices
	idxWrite += len2;

	// Write final data byte if COBSR is applicable
	if (cobsCode - 1 > (len + len2))
	{
		data[idxWrite++] = cobsCode;
	}
	return idxWrite;
}

/**
 * \brief Decode received frame in buffer - linear part
 *
 * This is a support function for rero_decode_frame_in_buffer

 * \return          Last COBS code found
 */
static uint8_t rero_decode_frame_in_buffer_lin_part(rero_t *hrero, size_t linLen, uint8_t *pBuff, uint8_t *escOccureFlag, uint8_t cobsCode, size_t *idxRead, size_t *idxWrite)
{
	size_t remLen = linLen - *idxRead;
	uint8_t cobsCodeOld;

	while ((cobsCode-1) < remLen)
	{
		// Conduct copy
		if (*escOccureFlag)
		{
			uart_dma_modify_rx(hrero->hudx, *idxWrite, &pBuff[*idxRead], cobsCode-1);
		}

		// Update indices
		*idxRead += cobsCode-1;
		*idxWrite += cobsCode-1;

		// Save new COBS code before it gets overwritten
		cobsCodeOld = cobsCode;
		cobsCode = pBuff[(*idxRead)++];

		// Add a zero to the end
		if (cobsCodeOld != 0xFF)
		{
			uart_dma_modify_byte_rx(hrero->hudx, (*idxWrite)++, 0);
		}
		else
		{
			*escOccureFlag = 1;
		}

		// Update remaining length
		remLen = linLen - *idxRead;
	}

	return cobsCode;
}

/**
 * \brief Decode received frame in buffer - linear finish
 *
 * This is a support function for rero_decode_frame_in_buffer

 * \return          Last write index
 */
static size_t rero_decode_frame_in_buffer_lin_finish(rero_t *hrero, size_t len, uint8_t *pBuff, uint8_t escOccureFlag, uint8_t cobsCode, size_t idxRead, size_t idxWrite)
{
	size_t remLen = len-idxRead;

	// Last COBS code reached - write remaining bytes and exit the loop
	if (escOccureFlag)
	{
		uart_dma_modify_rx(hrero->hudx, idxWrite, &pBuff[idxRead], remLen);
	}

	// Update indices
	idxWrite += remLen;

	// Write final data byte if applicable for COBSR
	if (cobsCode - 1 > remLen)
	{
		uart_dma_modify_byte_rx(hrero->hudx, idxWrite++, cobsCode);
	}

	return idxWrite;
}

/**
 * \brief Decode received frame in buffer - wrap
 *
 * This is a support function for rero_decode_frame_in_buffer

 * \return          Last COBS code found
 */
static uint8_t rero_decode_frame_in_buffer_wrap(rero_t *hrero, size_t len, uint8_t *pBuff, uint8_t *escOccureFlag, uint8_t cobsCode, size_t *idxRead, size_t *idxWrite)
{
	uint8_t cobsCodeOld;

	len -= *idxRead; 					// Determine remaining length

	// Copy first part
	if (*escOccureFlag)
	{
		uart_dma_modify_rx(hrero->hudx, *idxWrite, &pBuff[*idxRead], len);
	}

	// Update indices
	*idxRead = 0;
	*idxWrite += len;

	// Update pointer to buffer
	pBuff = hrero->hudx->huart->pRxBuffPtr;

	// Update length
	len = cobsCode - 1 - len;

	// Copy part two
	if (*escOccureFlag)
	{
		uart_dma_modify_rx(hrero->hudx, *idxWrite, pBuff, len);
	}

	// Update indices
	*idxRead += len;
	*idxWrite += len;

	// Update cobs code
	cobsCodeOld = cobsCode;
	cobsCode = pBuff[(*idxRead)++];

	// Add a zero to the end
	if (cobsCodeOld != 0xFF)
	{
		uart_dma_modify_byte_rx(hrero->hudx, (*idxWrite)++, 0);
	}
	else
	{
		*escOccureFlag = 1;
	}

	return cobsCode;
}

/**
 * \brief Decode received frame in buffer - wrap finish
 *
 * This is a support function for rero_decode_frame_in_buffer
 *
 * \return          Last write index
 */
static size_t rero_decode_frame_in_buffer_wrap_finish(rero_t *hrero, size_t len, uint8_t *pBuff, uint8_t escOccureFlag, uint8_t cobsCode, size_t idxRead, size_t idxWrite)
{
	size_t len2 = hrero->frameLen - len; 	// Determine remaining length of second part

	len -= idxRead; 						// Determine remaining length of first part

	// Copy first part
	if (escOccureFlag)
	{
		uart_dma_modify_rx(hrero->hudx, idxWrite, &pBuff[idxRead], len);
	}

	// Update indices
	idxRead = 0;
	idxWrite += len;

	// Update pointer to buffer
	pBuff = hrero->hudx->huart->pRxBuffPtr;

	// Copy part two
	if (escOccureFlag)
	{
		uart_dma_modify_rx(hrero->hudx, idxWrite, pBuff, len2);
	}

	// Update indices
	idxWrite += len2;

	// Write final data byte if COBSR is applicable
	if (cobsCode - 1 > (len + len2))
	{
		uart_dma_modify_byte_rx(hrero->hudx, idxWrite++, cobsCode);
	}
	return idxWrite;
}

/**
 * \brief Decodes a COBS encoded array of length < 254
 *
 * Decodes a COBS encoded frame of length < 254 intended to be used for header and CRC decoding. The frame is decoded in the buffer.
 *
 * \param[in]       hrero: 		Pointer to RX state machine structure
 * \param[in]       off:  		Offset where to start decoding in buffer. ATTENTION: First COBS code of the encoded frame must not be contained in the encoded frame! Adjust offset value accordingly!
 * \param[in]       len:  		Length of frame to decode in buffer. ATTENTION: EOF delimiter (0x00) must not be contained in the encoded frame! Adjust len accordingly!
 * \param[in]       cobsCode:  	First COBS code
 * \return          Remaining COBS code for later use (intended to first decode header and later decode CRC)
 */
static uint8_t rero_decode_cobs_tiny(rero_t *hrero, size_t off, size_t len, uint8_t cobsCode)
{
	if (cobsCode > len)
	{
		return cobsCode - len;
	}
	else
	{
		size_t idx = cobsCode - 1;
		size_t idxLast = idx;

		while (idx < len)
		{
			uart_dma_peek_byte_rx(hrero->hudx, off+idx, &cobsCode);
			idxLast = idx;
			uart_dma_modify_byte_rx(hrero->hudx, off+idx, 0);
			idx += cobsCode;
		}

		return cobsCode - (len - idxLast - 1);
	}
}

/**
 * \brief Convert from big to little endianess
 *
 * Helper function to convert an array of maximum 4 bytes to a little endianess format in buffer.
 *
 * \param[in]       hrero: 		Pointer to RX state machine structure
 * \param[in]       off:  		Offset where to start reading bytes
 * \param[in]       len:  		Number of bytes to convert into a big endianess value - Maximum is 4
 * \return          Converted value
 */
static size_t rero_big_to_little_endianess(rero_t *hrero, size_t off, size_t len)
{
	assert_param(off <= 4);
	assert_param(len <= 4);

	size_t cnt;
	size_t val = 0;
	for (cnt = 0; cnt < len; cnt++)
	{
		uart_dma_peek_byte_rx(hrero->hudx, cnt+off, ((uint8_t *) &val) + len - 1 - cnt);
	}

	return val;

	//	// Read out bytes
	//	uart_dma_peek_rx(hrero->hudx, off, ((uint8_t *)&val) + sizeof(uint32_t) - len, len);
	//
	//	// Reverse order
	//	val = __REV(val);
	//
	//	return val;
}

/**
 * \brief Strip all non-EOF delimiter from buffer including first EOF delimiter found
 *
 * Strip all non-EOF delimiter from buffer including first EOF delimiter found and return true if an EOF delimiter was found.
 *
 * \param[in]       hrero: 		Pointer to RX state machine structure
 * \return          1 if EOF delimiter was found
 */
static uint8_t strip_non_eof_delimiter(rero_t *hrero)
{
	uint8_t *pBuff = uart_dma_get_linear_block_address_rx(hrero->hudx, 0);
	size_t len = uart_dma_get_linear_block_length_rx(hrero->hudx, 0);
	size_t cnt;

	// First part
	for (cnt = 0; cnt < len; cnt++)
	{
		// If we found an EOF delimiter skip it and all bytes received before
		if (pBuff[cnt] == RERO_EOF)
		{
			uart_dma_flush_rx(hrero->hudx,cnt+1);
			return 1;
		}
	}

	// Skip all found if not finished yet
	uart_dma_flush_rx(hrero->hudx,len);

	// Second part - if not found before
	if (cnt == len)
	{
		pBuff = hrero->hudx->huart->pRxBuffPtr;
		len = uart_dma_get_linear_block_length_rx(hrero->hudx, len);
		for (cnt = 0; cnt < len; cnt++)
		{
			// If we found an EOF delimiter skip it and all bytes received before
			if (pBuff[cnt] == RERO_EOF)
			{
				uart_dma_flush_rx(hrero->hudx,cnt+1);
				return 1;
			}
		}
		uart_dma_flush_rx(hrero->hudx,len);
	}
	return 0; 	// Nothing found
}

/**
 * \brief Strip all EOF delimiter from buffer
 *
 * Strip all EOF delimiter from buffer returns true if a non-EOF delimiter was found (but does not strip this non-EOF delimiter).
 *
 * \param[in]       hrero: Pointer to RX state machine structure
 * \return          1 if non-EOF delimiter was found
 */
static uint8_t strip_eof_delimiter(rero_t *hrero)
{
	uint8_t *pBuff = uart_dma_get_linear_block_address_rx(hrero->hudx, 0);
	size_t len = uart_dma_get_linear_block_length_rx(hrero->hudx, 0);
	size_t cnt;

	// First part
	for (cnt = 0; cnt < len; cnt++)
	{
		// If we found a non-EOF delimiter skip all EOF-delimiter bytes received before
		if (pBuff[cnt] != RERO_EOF)
		{
			uart_dma_flush_rx(hrero->hudx,cnt);
			return 1;
		}
	}

	// Skip all found if not finished yet
	uart_dma_flush_rx(hrero->hudx,len);

	// Second part - if not found before
	if (cnt == len)
	{
		pBuff = hrero->hudx->huart->pRxBuffPtr;
		len = uart_dma_get_linear_block_length_rx(hrero->hudx, len);
		for (cnt = 0; cnt < len; cnt++)
		{
			// If we found an EOF delimiter skip it and all bytes received before
			if (pBuff[cnt] != RERO_EOF)
			{
				uart_dma_flush_rx(hrero->hudx,cnt);
				return 1;
			}
		}
		uart_dma_flush_rx(hrero->hudx,len);
	}
	return 0; 	// Nothing found
}

/**
 * \brief Encode byte
 *
 * Encode byte on the fly and put it into TX buffer. Once all bytes are encoded use rero_send(rero_t * hrero, uint32_t addr) to send encoded frame.
 *
 * \param[in]       hrero: 		Pointer to RX state machine structure
 * \param[in]       data: 		Payload byte
 * \return          RERO_ERROR_TX_NO_SPACE if no space in TX buffer available or RERO_OK otherwise
 */
rero_status_t rero_encode(rero_t * hrero, uint8_t data)
{
	// Setup frame header space if new frame is started
	if (hrero->searchLen == 0)
	{
		// Start of encoding - reserve space for management bytes and first COBS code of payload
		size_t headerLen = 1 + RERO_AddFrameFieldLen + RERO_FrameLenCheck + RERO_AddrFieldLen;

		// Advance write pointer in buffer - we do not insert anything right now
		if(uart_dma_reserve_space_tx(hrero->hudx, headerLen, &(hrero->headerBuffRef)) != UART_DMA_OK)
		{
			return RERO_ERROR_TX_NO_SPACE;
		}

		// Reserve space for first COBS code
		if(uart_dma_reserve_space_tx(hrero->hudx, 1, &(hrero->cobsCodeRef)) != UART_DMA_OK)
		{
			// Revert changes done to be able to start again correctly
			uart_dma_delete_tx(hrero->hudx, headerLen);
			return RERO_ERROR_TX_NO_SPACE;
		}

		// Set search length
		hrero->searchLen = 1;
	}

	// Encode

	// Check for zero byte
	if (data == 0)
	{
		// A zero byte was found - write COBS code
		uart_dma_put_by_ref_tx(hrero->hudx, 0, hrero->searchLen, hrero->cobsCodeRef);

		// Leave space for next COBS code and save its reference
		if(uart_dma_reserve_space_tx(hrero->hudx, 1, &(hrero->cobsCodeRef)) != UART_DMA_OK)
		{
			return RERO_ERROR_TX_NO_SPACE;
		}

		// Set search length
		hrero->searchLen = 1;
	}
	else
	{
		// Copy data byte into buffer
		if(uart_dma_put_tx(hrero->hudx, data) != UART_DMA_OK)
		{
			return RERO_ERROR_TX_NO_SPACE;
		}

		// Increase search length
		hrero->searchLen++;

		// Check for long string of non-zero bytes
		if (hrero->searchLen == 0xFF)
		{
			// Write out a length code of 0xFF
			uart_dma_put_by_ref_tx(hrero->hudx, 0, hrero->searchLen, hrero->cobsCodeRef);

			// Leave space for next COBS code and save its reference
			if(uart_dma_reserve_space_tx(hrero->hudx, 1, &(hrero->cobsCodeRef)) != UART_DMA_OK)
			{
				// Revert changes done to be able to start again correctly
				uart_dma_delete_tx(hrero->hudx, 1);
				hrero->searchLen--;

				return RERO_ERROR_TX_NO_SPACE;
			}

			// Set search length
			hrero->searchLen = 1;
		}
	}

	return RERO_OK;
}

/**
 * \brief 			Send frame
 *
 * 					Send encoded frame. To encode a frame prior to sending use rero_encode(rero_t * hrero, uint8_t data).
 *
 * Example usage:
 * \code{.c}
 *
  size_t cnt;

  for (cnt=0; cnt<sizeof(msg); cnt++)
  {
	  if(rero_encode(&hrero, msg[cnt]) != RERO_OK)
	  {
		  Error_Handler();
	  }
  }

  rero_send(&hrero, 5); 	// Send encoded frame to address 5
 * \endcode
 *
 * \param[in]       hrero: 		Pointer to RX state machine structure.
 * \param[in]       addr: 		Address to send the frame to. If RERO_AddrFieldLen was set to zero (no address field), this value is ignored
 * \return          RERO_ERROR_TX_NO_SPACE if no space in TX buffer available or RERO_OK otherwise.
 */
rero_status_t rero_send(rero_t * hrero, uint32_t addr)
{
	// Check if frame was encoded before
	if (hrero->searchLen > 0)
	{
		// Finalize encoding
		uint8_t data = uart_dma_peek_byte_by_ref_tx(hrero->hudx, UART_DMA_TX_BUFFER_LEN - 1, hrero->hudx->wAbsTX); 	// Get last written data byte

		// Check if COBSR is applicable
		if (data < hrero->searchLen)
		{
			// Encoding same as plain COBS
			uart_dma_put_by_ref_tx(hrero->hudx, 0, hrero->searchLen, hrero->cobsCodeRef);
		}
		else
		{
			// Special COBS/R encoding: length code is final byte and final byte is removed from data sequence
			uart_dma_put_by_ref_tx(hrero->hudx, 0, data, hrero->cobsCodeRef);

			// Delete last byte
			uart_dma_delete_tx(hrero->hudx, 1);
		}

		// Check if enough space for EOF delimiter and CRC is available (if required)
		if (uart_dma_free_tx(hrero->hudx) < RERO_CRCLen + 1)
		{
			return RERO_ERROR_TX_NO_SPACE;
		}

		// Determine length of encoded data payload
		size_t lenEncPayload = uart_dma_cnt_rel_to_ref_tx(hrero->hudx, 1 + RERO_AddFrameFieldLen + RERO_FrameLenCheck + RERO_AddrFieldLen, hrero->headerBuffRef);

		// Setup array for management bytes
		uint8_t manBytes[2 + RERO_AddFrameFieldLen + RERO_FrameLenCheck + RERO_AddrFieldLen + RERO_CRCLen]; 	// We reserve an extra byte at the beginning for the additional COBS code

		// Insert payload length into management bytes
		size_t lenEncPayloadRev = __REV(lenEncPayload);
		memcpy(&manBytes[1], (uint8_t *)&lenEncPayloadRev + (sizeof(size_t) - 1 - RERO_AddFrameFieldLen), 1 + RERO_AddFrameFieldLen);

		// Insert address into management bytes if required
#if RERO_AddrFieldLen > 0
		// Change byte order to big endian
		uint32_t addrRev = __REV(addr);
		memcpy(&manBytes[2 + RERO_AddFrameFieldLen + RERO_FrameLenCheck], ((uint8_t *)&addrRev) + (sizeof(uint32_t) - RERO_AddrFieldLen), RERO_AddrFieldLen);
#endif

		// Calculate CRC checksum of address field (if required) and payload
#if RERO_CRCLen > 0

		// For STM32F3
#if defined(STM32F303x8)

#if RERO_AddrFieldLen > 0
		HAL_CRC_Accumulate(hrero->hcrc, (uint32_t*) (((uint8_t *)&addrRev) + (sizeof(uint32_t) - RERO_AddrFieldLen)), RERO_AddrFieldLen);
#endif

		// Next process payload
		uint8_t *pBuff = uart_dma_get_linear_block_address_by_ref_tx(hrero->hudx, 1 + RERO_AddFrameFieldLen + RERO_FrameLenCheck + RERO_AddrFieldLen, hrero->headerBuffRef);
		size_t len = uart_dma_get_linear_block_length_by_ref_tx(hrero->hudx, 1 + RERO_AddFrameFieldLen + RERO_FrameLenCheck + RERO_AddrFieldLen, hrero->headerBuffRef);

		// If current # of bytes available is bigger than those to be parsed we need to clip
		if (len > lenEncPayload)
		{
			len = lenEncPayload;
		}

		// Conduct partial CRC for first linear block
		uint32_t crc = HAL_CRC_Accumulate(hrero->hcrc, (uint32_t*) pBuff, len);

		// Check if there is a wrap around necessary
		if(lenEncPayload > len)
		{
			// We know that we need the beginning of the TX buffer. Instead of calculating it we simply get it. This is a slightly nasty style here but simply faster.
			crc = HAL_CRC_Accumulate(hrero->hcrc, (uint32_t*) hrero->hudx->pBuffTX, lenEncPayload - len);
		}

		// Reverse byte order of CRC since big endianess is used for transmission
		crc = __REV(crc);
		memcpy(&manBytes[2 + RERO_AddFrameFieldLen + RERO_FrameLenCheck + RERO_AddrFieldLen], (uint8_t *)&crc + (sizeof(uint32_t) - RERO_CRCLen), RERO_CRCLen);
#endif

#if defined(STM32F411xE)

#if RERO_AddrFieldLen > 0
		// For the STM32F4 we need to compute CRC by CRC32 operations. For this we need all data bytes relevant for CRC in the TX buffer
		uart_dma_write_2_by_ref_tx(hrero->hudx, 1 + RERO_AddFrameFieldLen + RERO_FrameLenCheck, (((uint8_t *)&addrRev) + (sizeof(uint32_t) - RERO_AddrFieldLen)), RERO_AddrFieldLen, hrero->headerBuffRef);

		// Next process payload
		uint8_t *pBuff = uart_dma_get_linear_block_address_by_ref_tx(hrero->hudx, 1 + RERO_AddFrameFieldLen + RERO_FrameLenCheck, hrero->headerBuffRef);
		size_t len = uart_dma_get_linear_block_length_by_ref_tx(hrero->hudx, 1 + RERO_AddFrameFieldLen + RERO_FrameLenCheck, hrero->headerBuffRef);

		// If current # of bytes available is bigger than those to be parsed we need to clip
		if (len > lenEncPayload + RERO_AddrFieldLen)
		{
			len = lenEncPayload + RERO_AddrFieldLen;
		}

		uint32_t crc = CRC_handle_8_plus_wrap(hrero->hcrc, pBuff, hrero->hudx->pBuffTX, len, lenEncPayload + RERO_AddrFieldLen, CRC_COMPUTATION);
#else

		// Next process payload
		uint8_t *pBuff = uart_dma_get_linear_block_address_by_ref_tx(hrero->hudx, 1 + RERO_AddFrameFieldLen + RERO_FrameLenCheck + RERO_AddrFieldLen, hrero->headerBuffRef);
		size_t len = uart_dma_get_linear_block_length_by_ref_tx(hrero->hudx, 1 + RERO_AddFrameFieldLen + RERO_FrameLenCheck + RERO_AddrFieldLen, hrero->headerBuffRef);

		// If current # of bytes available is bigger than those to be parsed we need to clip
		if (len > lenEncPayload)
		{
			len = lenEncPayload;
		}

		uint32_t crc = CRC_handle_8_plus_wrap(hrero->hcrc, pBuff, hrero->hudx->pBuffTX, len, lenEncPayload + RERO_AddrFieldLen, CRC_COMPUTATION);
#endif // RERO_AddrFieldLen > 0

		// Reverse byte order of CRC since big endianess is used for transmission
		crc = __REV(crc);
		memcpy(&manBytes[2 + RERO_AddFrameFieldLen + RERO_FrameLenCheck + RERO_AddrFieldLen], (uint8_t *)&crc + (sizeof(uint32_t) - RERO_CRCLen), RERO_CRCLen);

#endif // defined(STM32F411xE)

#endif // RERO_CRCLen > 0

		uint8_t cnt;
		uint8_t *pTmp;

		// Determine length field checksum if required
#if RERO_FrameLenCheck == 1

		pTmp = (uint8_t *)(&lenEncPayload);
		manBytes[2 + RERO_AddFrameFieldLen] = 0;

		// Sum up
		for (cnt = 0; cnt < 1+RERO_AddFrameFieldLen; cnt++)
		{
			manBytes[2 + RERO_AddFrameFieldLen] += *pTmp++;
		}

		// Do one's complement
		manBytes[2 + RERO_AddFrameFieldLen] = ~manBytes[2 + RERO_AddFrameFieldLen];
#endif

		// Encode management bytes: length field + frame length check field + address field + CRC field
		uint8_t searchLen = 1;
		uint8_t cobsCodeIdx = 0;

		for (cnt=1; cnt < 2 + RERO_AddFrameFieldLen + RERO_FrameLenCheck + RERO_AddrFieldLen + RERO_CRCLen; cnt++)
		{
			if (manBytes[cnt] == 0)
			{
				// A zero byte was found - write COBS code
				manBytes[cobsCodeIdx] = searchLen;

				// Save index of next COBS code
				cobsCodeIdx = cnt;

				// Reset search length
				searchLen = 1;
			}
			else
			{
				// Increase search length
				searchLen++;
			}
		}

		// Write last COBS code
		manBytes[cobsCodeIdx] = searchLen;

		// Combine first two bytes - this is always possible by construction of RERO
		manBytes[1] |= (manBytes[0] << (8 - RERO_FirstCobsCodeNBits));

		// Put all in buffer
		uart_dma_write_2_by_ref_tx(hrero->hudx, 0, &manBytes[1], 1 + RERO_AddFrameFieldLen + RERO_FrameLenCheck + RERO_AddrFieldLen, hrero->headerBuffRef);

#if RERO_CRCLen > 0
		uart_dma_write_2_tx(hrero->hudx, &manBytes[2 + RERO_AddFrameFieldLen + RERO_FrameLenCheck + RERO_AddrFieldLen], RERO_CRCLen);
#endif
		// Add EOF delimiter
		uart_dma_put_tx(hrero->hudx, RERO_EOF);

		// Send data
		uart_dma_send(hrero->hudx);

		// Reset search length to indicate rero_encode() to start a new frame
		hrero->searchLen = 0;
	}

	return RERO_OK;
}

#if defined(STM32F411xE)

/**
 * @brief  Enter 8-bit input data to the CRC calculator.
 *         Specific data handling to optimize processing time.
 * @param  hcrc CRC handle
 * @param  pBuffer pointer to the input data buffer
 * @param  BufferLength input data buffer length
 * @retval uint32_t CRC (returned value LSBs for CRC shorter than 32 bits)
 */
static uint32_t CRC_handle_8_plus_wrap(CRC_HandleTypeDef *hcrc, uint8_t *pBuffLin, uint8_t *pBuffWrap, size_t lenLin, size_t lenTot, rero_crc_t type)
{
	// Since the CRC module of the STM32F4 only works with 32 bit words, we need to process CRC stuff in 4 byte packages
	// Conduct for 4 byte slices
	size_t cnt;

	for(cnt = 0; cnt < lenLin/4; cnt++)
	{
		hcrc->Instance->DR = __REV(((uint32_t *)pBuffLin)[cnt]);
	}

	// Handle wrap around
	uint8_t rem = lenLin - sizeof(uint32_t)*cnt;
	lenLin = lenTot - lenLin;

	// if (lenTot - sizeof(uint32_t)*cnt) > sizeof(uint32_t) -> complete wrap

	if (rem > 0)
	{
		pBuffLin += sizeof(uint32_t)*cnt;
		uint8_t cnt2;
		uint32_t wrapData = 0;
		for (cnt2 = 0; cnt2 < rem; cnt2++)
		{
			((uint8_t *) &wrapData)[cnt2] = pBuffLin[cnt2];
		}

		// Look for wrapped part
		for (; cnt2 < sizeof(uint32_t) && lenLin > 0; cnt2++)
		{
			((uint8_t *) &wrapData)[cnt2] = *pBuffWrap++;
			lenLin--;
		}

		// Put into CRC - for verification we can simply use the CRC32 but for computation we need to use the software implementation if wrapData was not filled completely
		if (type == CRC_VERIFICATION || cnt2 == sizeof(uint32_t))
		{
			hcrc->Instance->DR = __REV(wrapData);

			// Check if we are already done - if cnt2 != sizeof(uint32_t) then there was not sufficient data to completely fill wrapData, which means we treated all data and can return
			if (cnt2 < sizeof(uint32_t))
			{
				return hcrc->Instance->DR;
			}
		}
		else
		{
			// We can be sure to have data bytes treated when we reach this line - compute remaining CRC and return
			uint32_t crc = hcrc->Instance->DR;
			for (cnt = 0; cnt < cnt2; cnt++)
			{
				crc = CRC_soft(crc, ((uint8_t *) &wrapData)[cnt]);
			}
			return crc;
		}
	}

	// Process rest
	for(cnt = 0; cnt < lenLin/4; cnt++)
	{
		hcrc->Instance->DR = __REV(((uint32_t *)pBuffWrap)[cnt]);
	}

	rem = lenLin - sizeof(uint32_t)*cnt;
	if (rem > 0)
	{
		pBuffWrap += sizeof(uint32_t)*cnt;
		uint8_t cnt2;
		uint32_t wrapData = 0;
		for (cnt2 = 0; cnt2 < rem; cnt2++)
		{
			((uint8_t *) &wrapData)[cnt2] = pBuffWrap[cnt2];
		}

		// Put into CRC - for verification we can simply use the CRC32 but for computation we need to use the software implementation if wrapData was not filled completely
		if (type == CRC_VERIFICATION || cnt2 == sizeof(uint32_t))
		{
			hcrc->Instance->DR = __REV(wrapData);
		}
		else
		{
			// We can be sure to have data bytes treated when we reach this line - compute remaining CRC and return
			uint32_t crc = hcrc->Instance->DR;

			for (cnt = 0; cnt < cnt2; cnt++)
			{
				crc = CRC_soft(crc, ((uint8_t *) &wrapData)[cnt]);
			}
			return crc;
		}
	}

	return hcrc->Instance->DR;
}

static uint32_t CRC_soft(uint32_t crc, uint8_t data)
{
	uint8_t cnt;
	uint32_t msb;

	// xor next byte to upper bits of crc
	crc ^= (((uint32_t)data) << 24);
	for (cnt = 0; cnt < 8; cnt++){
		msb = crc>>31;
		crc <<= 1;
		crc ^= (0 - msb) & 0x04C11DB7;
	}

	return crc;
}

#endif

/** @} */
