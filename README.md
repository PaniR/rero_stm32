# Rero C Implementation for STM32 MCUs

This is the C implementation for the fast and efficient rero transmission protocol. It is tailored for the STM32 MCUs, however, may also be ported to different MCU types. A Python implementation is also [available](https://pypi.org/project/rero/). The implementation here is written for the NUCLEO-F303K8 development board.

*Rero* is an efficient communication protocol intended for transmissions of byte frames over e.g. a serial interface or buses.
It builds upon [COBS](https://en.wikipedia.org/wiki/Consistent_Overhead_Byte_Stuffing) which allows for a minimum encoding overhead.
It is perfectly suited for MCUs with DMA support.

Did somebody asked what is it good for? Here are some qualities of Rero:

| Quality     | How                          | Good for                             |
|-------------|------------------------------|--------------------------------------|
| Reliable    | CRC check                    | Checking integrity of frame          |
| Robust      | [COBS](https://en.wikipedia.org/wiki/Consistent_Overhead_Byte_Stuffing) and [COBSR](https://github.com/cmcqueen/cobs-c) byte stuffing           | Easy to resynchronize byte stream    |
| Efficient   | Exploit properties of COBS and COBSR   | Minimum processing overhead          |
| Scalable    | Customize Rero to your needs | Minimize frame overhead              |
| Bus-capable | Optional address field       | Address frame for specific recipient |

The C implementation of rero builds upon the efficient ring buffer implementation UART DMA !!!REFERENCE!!!. As the name suggests it is a ring buffer implementation tailored to work with the UART interface utilizing DMAs for receiving and transmitting. The buffer implementation is thread safe, does not need to disable any interrupts, is able to detect buffer overflows with overwhelming probability, and does not waste the last byte of the buffer to know if the buffer is full or empty (which is the case for the traditional ring buffer implementations). In case the buffer length is a power of two, required modulo operations are conducted efficiently.  

## Quick start

Follow the instruction in [rero_setup.pdf](https://gitlab.com/PaniR/rero_stm32).

## Usage

### Example usage for encoding and sending

```c
size_t cnt;

for (cnt=0; cnt<sizeof(msg); cnt++)
{
	if(rero_encode(&hrero, msg[cnt]) != RERO_OK)
	{
		Error_Handler(); 	// Your error handling
	}
}

rero_send(&hrero, 5); 	// Send encoded frame to address 5
```

### Example usage for receiving

```c

// Some support variables
rero_status_t reroStat;
size_t len1, len2;
void * p1;
void * p2;
uint8_t test_buffer[UART_DMA_RX_BUFFER_LEN];
uint8_t test_buffer2[UART_DMA_RX_BUFFER_LEN];
uint8_t test_buffer3[UART_DMA_RX_BUFFER_LEN];
uint8_t * pBuff;

// Conduct rero receive state machine
reroStat = rero_check_rx(&hrero);

if(reroStat == RERO_ERROR_SYNC)
{
	Error_Handler(); 	// Your error handling
}

// Check for possible occuring events
if(reroStat == RERO_OTHER_REC)
{
	Error_Handler(); 	// Your error handling
}

if(reroStat == RERO_ERROR_CRC)
{
	Error_Handler(); 	// Your error handling
}

if(reroStat == RERO_ERROR_TIMEOUT)
{
	Error_Handler(); 	// Your error handling
}

// Check if a frame was received
if(rero_is_frame_available(&hrero))
{
	// If needed, get length of decoded frame before decoding it 
	// (this is not required for the decoding process but maybe for you for later processing)
	frameLen = rero_get_decoded_frame_length(&hrero);

	// Optional: Decode received frame in the buffer. This is most economically 
	// if received frame is read multiple times as decoding is done only once. 
	// Since decoded in buffer this option uses the fewest number of decoding 
	// steps (very few bytes are copied).
	rero_decode_frame_in_buffer(&hrero);

	// First option: Decode frame in buffer and get pointer and corresponding 
	// length to decoded frame in buffer. Perfect for e.g. further DMA transmission. 
	// Since it is a ring buffer we have two pointers in case of a wrap around.
	rero_get_linear_read_info(&hrero, &len1, &len2, (void *) &p1, (void *) &p2);
	if(len1 > 0)
	{
		memcpy((void*) test_buffer1, (void*) p1, len1);
		if(len2 > 0)
		{
			memcpy((void*) (test_buffer1 + len1), (void*) p2, len2);
		}
	}

	// Second option: Copy received frame to test buffer. This is useful if you 
	// need the data in your own array and not in the buffer. Decoding is done 
	// on the fly if frame was not already decoded in buffer.
	rero_copy_frame(&hrero, (void*) test_buffer2);

	// Third option: Decode and consume frame byte-wise. This is the best option
	// if you can consume your frame byte-wise.
	// Example byte-wise consumption: Decode message received, encode it back on 
	// the fly and send it back
	while(rero_iterate(&hrero, &data))
	{
		if(rero_encode(&hrero, data) != RERO_OK)
		{
			Error_Handler();
		}
	}

	// Send encoded frame back to address 99
	rero_send(&hrero, 99);

	// Give space free in ring buffer and allow for next frame to be processed
	rero_notify_frame_processed(&hrero);
}
```

## Documentation

The API documentation of the C implementation can be found [here](https://panir.gitlab.io/rero_stm32).
For more information how rero works please have a look at the Python documentation [here](https://panir.gitlab.io/rero/docs/User%20guide/configuration/).

## Contribution
For information on how to contribute to the project, please check the [Contributor's Guide](CONTRIBUTING.md).

## Contact
Please use the [Gitlab service desk](incoming+panir-rero-stm32-14676440-issue-@incoming.gitlab.com) or if you have a Gitlab account you may directly open an [issue](https://gitlab.com/PaniR/rero_stm32/issues).

## License
[MIT License](LICENSE.md)
